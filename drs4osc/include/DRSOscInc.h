/*
 * DRSOscInc.h
 * Collection of all DRS oscilloscope header include files
 * $Id: DRSOscInc.h 20924 2013-03-21 13:31:33Z ritt $
 */
#ifndef __OSCI_INC_H__
#define __OSCI_INC_H__

#define MAX_N_BOARDS 16
#define MAX_N_FILTERSAMPLES 8192

#include "wx/wx.h"
#include "wx/timer.h"
#include "wx/dcbuffer.h"
#include "wx/print.h"
#include "wx/numdlg.h"
#include "wx/log.h"

#include "drs_core/mxml.h"
#include "drs_core/DRS.h"

#include "DRSOsc.h"
#include "Osci.h"
#include "Measurement.h"
#include "ConfigDialog.h"
#include "DisplayDialog.h"
#include "AboutDialog.h"
#include "InfoDialog.h"
#include "MeasureDialog.h"
#include "TriggerDialog.h"
#include "DOScreen.h"
#include "VoltageScanDialog.h"

// Corvus Control Includes
// #include "wx/socket.h"
// #include "ConnectCrvs.h"
//----------------------//

#include "DOFrame.h"
#include "EPThread.h"

double ss_nan();
int ss_isnan(double x);
void ss_sleep(int ms);

#endif
