#pragma once

#include "DRSOsc.h"

#include "drs_core/power_supply.h"
#include "drs_core/tcp_connector.h"
#include "drs_core/mxml.h"

#include <memory>
#include <exception>
#include <iomanip>

#include <wx/thread.h>
#include <wx/log.h>
#include <wx/timer.h>

class DoFrame;
class Osci;

namespace run_control
{
    enum class RCState
    {
        DISCONNECTED = 0,
        CONNECTED = 1,
        CONFIGURE = 2,
        RUNNING = 3,
        RAMPING = 4,
        FUCK_MSVC = 5, // MSVC does not allow enum class label ERROR, read the fucking iso standard!
    };

    class RCError : public std::runtime_error
    {
    public:
        RCError(const std::string &msg) noexcept;
        ~RCError() override = default;
    };

    class RunControl
    {
    public:
        RunControl() noexcept = default;

        auto Connect(const std::string &ip, int16_t port) -> power_supply::rpc::CurrentState;
        auto Disconnect() -> void;
        auto GetPSState() -> power_supply::rpc::CurrentState;
        auto Configure(const power_supply::rpc::Configure &cfg) -> void;
        auto ChangeVoltage(const power_supply::rpc::ChangeVoltage &cfg) -> void;
        auto Stop() -> void;

        inline auto GetRCState() const noexcept -> RCState
        {
            wxCriticalSectionLocker guard(m_lock);
            return m_state;
        }

    private:
        std::unique_ptr<power_supply::Controller> m_ps = nullptr;
        RCState m_state = RCState::DISCONNECTED;
        mutable wxCriticalSection m_lock;
    };
} // namesapce run_control

struct VSFileDependency
{
    MXML_WRITER **xml_writer = nullptr; // xml file
    int *file_descriptor = nullptr;     // file desciptor used for binary and csv. ROOT file not working!
    int *file_type = nullptr;           // file type
    Osci *osci = nullptr;               // osci to reset the serial event, ie Osci::SetEventSerial(1);
};

struct VSRunDependency
{
    wxCriticalSection **gui_lock = nullptr; // global lock used between the osci, epthread and the gui
    VSFileDependency file_dependency{};     // the main control hack in the epthread
    int *max_events = nullptr;              // exit condition hack: current events has to  be smaller then m_max events...
    int *cur_events = nullptr;              // current number of saved events
};

struct VSRunConfiguration
{
    float start_voltage = 0;
    float end_voltage = 0;
    float step_size = 0;
    int events_per_step = 0;
    int cur_step = 0;

    inline auto steps() const noexcept -> int
    {
        auto steps = static_cast<int>(std::abs(end_voltage - start_voltage) / step_size);
        steps += end_voltage > (start_voltage + steps * step_size);
        return steps;
    }

    inline auto next_voltage(int step) const noexcept -> float
    {
        auto next_v = (start_voltage + step * step_size);
        return end_voltage >= next_v ? next_v : end_voltage;
    }

    inline auto max_events() const noexcept -> int
    {
        return this->steps() * events_per_step;
    }
};

struct VSOutputConfiguration
{
    std::string prefix;
    std::string postfix;

    inline auto filename(float cur_voltage) const -> std::string
    {
        auto name = std::stringstream();
        name << prefix << '_' << std::scientific << cur_voltage << 'V' << postfix;
        return name.str();
    }
};

/** Implementing VoltageScan_fb */
class VoltageScanDialog : public VoltageScan_fb
{
private:
    using Controller = power_supply::Controller;
    using ChangeVoltage = power_supply::rpc::ChangeVoltage;
    using Configure = power_supply::rpc::Configure;
    using CurrentState = power_supply::rpc::CurrentState;
    using RPCError = power_supply::rpc::RPCError;
    using TCPError = tcp_socket::TCPError;

    static constexpr auto QUERY_RATE = 1000; // query rate if connected in [ms].

protected:
    auto OnVSConnect(wxCommandEvent &event) -> void override;

    auto OnVSDisconnect(wxCommandEvent &event) -> void override;

    auto OnVSSave(wxCommandEvent &event) -> void override;

    auto OnVSClose(wxCommandEvent &event) -> void override;

    auto OnTimerUpdate(wxTimerEvent &event) noexcept -> void;

public:
    VoltageScanDialog(wxWindow *parent, VSRunDependency &&external_dependencies) noexcept;

    auto StopOnOsciError(int osci_status) noexcept -> void;
    auto SkipWhilePSInTransition() noexcept -> bool;
    auto ConfigureNextVoltage(int saved_events) noexcept -> void;
    inline auto IsPSStateInError() const noexcept -> bool
    {
        if (!m_ctrl.get())
            return false;
        return m_ctrl->GetRCState() == run_control::RCState::FUCK_MSVC;
    }
    auto StopOnPSError() noexcept -> void {
        if (!this->IsPSStateInError()) return void();

        for (auto i_retry = 0; i_retry < 3; ++i_retry) 
            try {
                this->StopRun();
                this->CloseOpenedFile();
                return void();
            } catch (const run_control::RCError& err) {
                wxLogError(err.what());
            }
    }

private:
    auto InitGUI() noexcept -> void;

    inline auto GetIP() const noexcept -> std::string
    {
        return m_txtIP->GetValue().ToStdString();
    }

    inline auto GetPort() const noexcept -> int
    {
        return static_cast<int16_t>(stoi(m_txtPort->GetValue().ToStdString()));
    }

    auto GetRunCfg() -> VSRunConfiguration;
    auto GetPSConfig() -> Configure;
    auto StartRun() noexcept -> bool;
    auto StopRun() noexcept -> void;

    auto UpdateConnected(const CurrentState &state) noexcept -> void;

    auto UpdateDisconnected() noexcept -> void;

    auto OpenCSV(float voltage) const -> void;
    auto OpenXML(float voltage) const -> void;
    auto OpenBinary(float voltage) const -> void;
    auto CloseOpenedFile() const noexcept -> void;

    auto SetExitCondition() const noexcept -> void;

private:
    VSRunDependency m_dependencies{};
    wxTimer *m_timer = nullptr;
    std::unique_ptr<run_control::RunControl> m_ctrl = nullptr;
    std::string m_ip = std::string();
    VSOutputConfiguration m_file_cfg;
    VSRunConfiguration m_run_cfg;
    int16_t m_port{0};
};
