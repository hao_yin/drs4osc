#pragma once

#include "drs_core/strlcpy.h"
#include "drs_core/DRS.h"

typedef struct
{
   int error;
   float wavemax, wavemin;
   double time_std, wave_std;
   float ppkavrg2;
   float ppkstd2;
} avrgmeas;
/*------------------------------------------------------------------*/
class DRS4exam
{
   int i, j, nBoards;
   DRS *drs;
   DRSBoard *b;
   int m_triggercell;
   unsigned char m_writeSR;
   float m_time_array[4][1024];
   float m_wave_array[4][1024];
   double m_time_std_array[1024];
   double m_wave_std_array[1024];

   float m_ppkval = 0;
   double m_ppkvalstd;
   double m_timestd;
   float m_ppkavrg2;
   float m_ppkstd2;

   bool m_tcalon = true;
   bool m_rotated = true;
   bool m_calibrated = true;
   bool m_calibrated2 = true;

   int m_triggerchannel = 0;
   int m_measurechannel = 0;
   bool m_removespikes = false;
   float m_triggerlevel = 0.01f;

public:
   float m_time_avg_array[1024];
   float m_wave_avg_array[1024];

   DRS4exam() {}
   ~DRS4exam();
   bool Init();
   void SetExternalTrigger();
   void SetTriggerChannel(int channel, bool negedge = false);
   int GetTriggerSource();
   void SetTriggerLvl(float level);     // trigger level in mV
   void SetMeasureChannel(int channel); // set channel for measurement
   int StartMeasurement(bool printout);
   void SetTcal(bool);
   avrgmeas StartAveragerMeas(int nMeasurements, bool printout = false);
   int StartPeakPeakMeas(int nMeasurements, bool printout = false);
   float GetPeakPeakMeasVal() { return m_ppkval; };
   void SetRemoveSpikes(bool flag) { m_removespikes = flag; };
   float GetTriggerLevel() { return m_triggerlevel; };
   double GetWavestdMeasVal() { return m_ppkvalstd; };
   double GetTimestdMeasVal() { return m_timestd; };
   double GetPeakPeakAvrgVal() { return m_ppkavrg2; };
   double GetPeakPeakStdVal() { return m_ppkstd2; };
   void RemoveSpikes();
};
