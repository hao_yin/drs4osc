///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.9.0 Jan 12 2021)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////
#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/sizer.h>
#include <wx/combobox.h>
#include <wx/checkbox.h>
#include <wx/panel.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////

#define ID_SERVER 1000
#define ID_CORVUSSEND 1001

///////////////////////////////////////////////////////////////////////////////
/// Class frameMain
///////////////////////////////////////////////////////////////////////////////
class frameMain : public wxFrame
{
private:
protected:
	wxPanel *m_panelMain;
	wxStaticText *m_statictextsrvip;
	wxTextCtrl *m_textCtrlserverip;
	wxStaticText *m_staticTextsrvport;
	wxTextCtrl *m_textCtrlserverport;
	wxButton *m_btnConnectSrv;
	wxStaticText *m_staticTextsendcommand;
	wxTextCtrl *m_textCtrlsendcrvcommand;
	wxButton *m_btnSendCrvCommand;
	wxStaticText *m_staticText29;
	wxStaticText *m_staticText4;
	wxTextCtrl *m_textCtrlSrvLog;
	wxStaticText *m_staticText30;
	wxTextCtrl *m_textCtrlDrs4Status;
	wxStaticText *m_staticText31;
	wxTextCtrl *m_textCtrlDRS4val;
	wxStaticText *m_staticText322;
	wxStaticText *m_staticText311;
	wxTextCtrl *m_textCtrlDRS4std;
	wxStaticText *m_staticText32;
	wxButton *m_btnSetTriggerLvl;
	wxTextCtrl *m_textCtrlDRS4setTrigVal;
	wxStaticText *m_staticText321;
	wxStaticText *m_staticText33;
	wxComboBox *m_cmboTriggerChannel;
	wxStaticText *m_staticText34;
	wxCheckBox *m_chkboxApply100Mhzsig;
	wxStaticText *m_staticText331;
	wxComboBox *m_cmboMeasureChannel;
	wxStaticText *m_staticText3312;
	wxComboBox *m_cmboNumAvrgMeas;
	wxStaticText *m_staticText3311;
	wxButton *m_btnGetDRSMeasure;
	wxStaticText *m_staticText33111;
	wxCheckBox *m_chkboxRemoveSpikes;
	wxStaticText *m_staticText37;
	wxStaticText *m_staticText5134;
	wxStaticText *m_staticText5132;
	wxStaticText *m_staticText5133;
	wxStaticText *m_staticText51331;
	wxTextCtrl *m_textCtrlXPos;
	wxStaticText *m_staticText44;
	wxTextCtrl *m_textCtrlYPos;
	wxStaticText *m_staticText441;
	wxTextCtrl *m_textCtrlZPos;
	wxStaticText *m_staticText442;
	wxTextCtrl *m_textCtrlStatus;
	wxStaticText *m_staticText28;
	wxButton *m_btnLoadPosintoStart;
	wxCheckBox *m_checkBoxXuse;
	wxStaticText *m_staticText53;
	wxTextCtrl *m_textCtrlXfrom;
	wxStaticText *m_staticText51;
	wxTextCtrl *m_textCtrlXto;
	wxStaticText *m_staticText513;
	wxTextCtrl *m_textCtrlXstepsize;
	wxStaticText *m_staticText514;
	wxCheckBox *m_checkBoxYuse;
	wxStaticText *m_staticText5;
	wxTextCtrl *m_textCtrlYfrom;
	wxStaticText *m_staticText512;
	wxTextCtrl *m_textCtrlYto;
	wxStaticText *m_staticText5131;
	wxTextCtrl *m_textCtrlYstepsize;
	wxStaticText *m_staticText5141;
	wxCheckBox *m_checkBoxZuse;
	wxStaticText *m_staticText52;
	wxTextCtrl *m_textCtrlZfrom;
	wxStaticText *m_staticText511;
	wxTextCtrl *m_textCtrlZto;
	wxStaticText *m_staticText51311;
	wxTextCtrl *m_textCtrlZstepsize;
	wxStaticText *m_staticText51411;
	wxButton *m_btnScanAxis;
	wxButton *m_btnMoveStart;
	wxMenuBar *m_menubarMain;
	wxMenu *m_menufile;
	wxMenu *m_menuedit;
	wxMenu *m_menuhelp;
	wxStatusBar *m_statusBar1;

	// Virtual event handlers, override them in your derived class
	virtual void ServerConCloseCheck(wxCommandEvent &event) { event.Skip(); }
	virtual void CorvusSendbtn(wxCommandEvent &event) { event.Skip(); }
	virtual void OnSetTrigLvlbtn(wxCommandEvent &event) { event.Skip(); }
	virtual void OnComboSetTriggerChannel(wxCommandEvent &event) { event.Skip(); }
	virtual void OnApply100MhzSig(wxCommandEvent &event) { event.Skip(); }
	virtual void OnComboSetMeasureChannel(wxCommandEvent &event) { event.Skip(); }
	virtual void OnComboSetNumAvrgMeas(wxCommandEvent &event) { event.Skip(); }
	virtual void OnDRSGetMeasure(wxCommandEvent &event) { event.Skip(); }
	virtual void OnRemoveSpikes(wxCommandEvent &event) { event.Skip(); }
	virtual void OnLoadPostoStartbtn(wxCommandEvent &event) { event.Skip(); }
	virtual void ScanAxis(wxCommandEvent &event) { event.Skip(); }
	virtual void MoveStart(wxCommandEvent &event) { event.Skip(); }
	virtual void OnExit(wxCommandEvent &event) { event.Skip(); }
	virtual void OnShowPlot(wxCommandEvent &event) { event.Skip(); }
	virtual void OnAbout(wxCommandEvent &event) { event.Skip(); }

public:
	frameMain(wxWindow *parent, wxWindowID id = wxID_ANY, const wxString &title = wxT("Corvus Server Connect"), const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxSize(1000, 700), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL);

	~frameMain();
};

///////////////////////////////////////////////////////////////////////////////
/// Class framePlot
///////////////////////////////////////////////////////////////////////////////
class framePlot : public wxFrame
{
private:
protected:
	wxPanel *m_panelplotthis;
	wxButton *m_btnplotexit;

	// Virtual event handlers, override them in your derived class
	virtual void OnClose(wxCommandEvent &event) { event.Skip(); }

public:
	framePlot(wxWindow *parent, wxWindowID id = wxID_ANY, const wxString &title = wxT("DRS4 Signal"), const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxSize(715, 518), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL);

	~framePlot();
};
