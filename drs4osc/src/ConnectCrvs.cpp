/**
    Corvus Control Connect to Server Dialog

    This connects to the RAW TCP Server of the
    LabView Corvus Control.
*/

#include "DRSOscInc.h"

#include "wx/socket.h"
#include "wx/event.h"
#include "wx/textfile.h"
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/timer.h>

using namespace std;

// --------------------------------------------------------------------------
// event tables and other macros for wxWidgets
// --------------------------------------------------------------------------

wxBEGIN_EVENT_TABLE(ControlCrvs, wxEvtHandler)
    //    EVT_MENU(wxID_EXIT ,  MyFrame::OnExit )
    //    EVT_TIMER(ID_TIMER,   MyFrame::OnTimer)
    EVT_SOCKET(ID_SOCKET_CRVS, ControlCrvs::OnSocketEvent)
    //    EVT_THREAD(ID_WORKER_THREAD, MyFrame::OnWorkerEvent)
    //    EVT_THREAD(ID_LOG_THREAD,    MyFrame::OnLogEvent)
    //    EVT_THREAD(ID_START_THREAD,  MyFrame::OnThreadStart)
    //    EVT_THREAD(ID_FINISH_THREAD, MyFrame::OnThreadCompletion)
    //    EVT_THREAD(ID_STARTMEAS_THREAD, MyFrame::OnThreadStartMeas)
    wxEND_EVENT_TABLE()

        wxBEGIN_EVENT_TABLE(LogandsendCmdCrvsDialog, wxEvtHandler)
            EVT_TIMER(ID_TIMER_LOGRELOAD, LogandsendCmdCrvsDialog::OnTimerLogFileReload)
                wxEND_EVENT_TABLE()

                    ControlCrvs::ControlCrvs(wxWindow *parent)
{
    m_frame = (DOFrame *)parent;
    m_osci = m_frame->GetOsci();

    // redirect log messages to file
    logfile.open("Corvuslog.txt");
    logger = new wxLogStream(&logfile);
    wxLog::SetActiveTarget(logger);

    // m_psocket = m_frame->GetCrvsSocket();

    // Create the socket
    m_socket = new wxSocketClient();

    // Setup the event handler and subscribe to most events
    m_socket->SetEventHandler(*this, ID_SOCKET_CRVS);
    m_socket->SetNotify(wxSOCKET_CONNECTION_FLAG |
                        wxSOCKET_INPUT_FLAG |
                        wxSOCKET_LOST_FLAG);
    m_socket->Notify(true); // wxwidgets change color button
}

ControlCrvs::~ControlCrvs()
{
    if (m_socket->IsConnected())
    {
        m_socket->Close();
        while (!m_socket->IsDisconnected())
            ;
    }
    m_socket->Destroy();
    m_socket = NULL;

    wxLog::SetActiveTarget(NULL);
    delete logger;
    logfile.close();
}

bool ControlCrvs::ConnectServer(wxString hostname, wxString port)
{
    if (!m_socket->IsConnected())
    {

        wxIPV4address addr;
        int iport = wxAtoi(port);

        addr.Hostname(hostname);
        addr.Service(iport);
        wxLogMessage("Trying to connect to %s:%d", hostname, addr.Service());
        // Wait for the connection event
        m_socket->Connect(addr, false);
        // Wait until the
        // request completes or until we decide to give up
        bool waitmore = true;
        // wait 3sec for connection
        while (!m_socket->WaitOnConnect(3) && waitmore)
        {
            // possibly give some feedback to the user,
            // and update waitmore as needed.
        }
        if (!m_socket->IsConnected())
        {
            wxMessageBox("Cannot connect to server!!!", "Connection Error", wxICON_ERROR);
            // wxLogMessage("Cannot connect to server.");
            return true;
        }
        else
        {
            wxMessageBox("Connected to server", "Connection Established",
                         wxICON_INFORMATION);
            return false;
        }
    }
    wxLogMessage("Server already connected");
    return true;
}

void ControlCrvs::OnSocketEvent(wxSocketEvent &event)
{
    // The socket that had the event
    wxSocketBase *sock = event.GetSocket();
    // Common buffer shared by the events
    char buf[128];
    switch (event.GetSocketEvent())
    {
    case wxSOCKET_CONNECTION:
    {
        wxLogMessage("... socket is now connected.");

        // m_timer->Start(TIMEASKPOS);
        m_frame->UpdateStatusBar();
        break;
    }
    case wxSOCKET_INPUT:
    {
        memset(buf, 0, sizeof(buf));
        sock->Read(buf, sizeof(buf));
        // printf("%s\n",buf); fflush(stdout);
        wxString s = wxString::FromUTF8(buf);
        s.Trim();

        wxLogMessage("Server sent: %s", s);
        /* ofstream logfile("Corvuslog.txt");
         logfile << "Server sent: " << s << endl;
         logfile.close();*/

        s << ',';

        //// POS? gests back: x.xxx,x.xxx,x.xxx,x
        // if(m_getpos) {
        //     bool error = false;
        //     // get actual position request format is: <xpos> <ypos> <zpos>
        //     wxStringTokenizer token(s, ",");
        //     // todo check if has 3 entries
        //     int i=0;
        //     wxTextCtrl* wxtextfields[] = {m_textCtrlXPos, m_textCtrlYPos, m_textCtrlZPos};
        //     while ( token.HasMoreTokens() && i<3) {
        //         wxString subs = token.GetNextToken();
        //         // wxCharBuffer buffer = subs.ToUTF8();
        //         // printf("positions: axis %d  %s\n", i, buffer.data() );
        //         // fflush(stdout);
        //         double val;
        //         if( !subs.ToDouble( &val ) ) {
        //             error = true;
        //             break;
        //         }
        //         m_fxyzpos[i] = val;
        //         m_xyzpos[i] = int(m_fxyzpos[i]);
        //         wxtextfields[i]->Clear();
        //         (*(wxtextfields[i])) << subs;
        //         i++;
        //     }
        //     // next token is the Corvu status
        //     if (token.HasMoreTokens() && !error ) {
        //         wxString subs = token.GetNextToken();
        //         m_status = wxAtoi(subs);
        //         m_textCtrlStatus->Clear();
        //         *m_textCtrlStatus << subs;
        //     }
        //     m_getpos = false;
        //
        // } else if(m_scanongoing) {
        // } else {
        //     wxLogMessage("Server sent len=%d: \n\t%s", int(s.Len()), buf );
        // }
        break;
    }
    // The server hangs up after sending the data
    case wxSOCKET_LOST:
    {
        /* ofstream logfile("Corvuslog.txt");
         logfile << "Connection to server lost" << endl;
         logfile.close();*/
        wxLogMessage("Connection to server lost");

        m_frame->UpdateStatusBar();
        break;
    }
    default:
        /* ofstream logfile("Corvuslog.txt");
         logfile << "Unknown socket event!!!" << endl;
         logfile.close();*/
        wxLogMessage("Unknown socket event!!!");
        break;
    }
}

void ControlCrvs::CorvusSendCmd(wxString s, bool printdebug)
{
    m_sendok = false;
    // adding "\r\n" for LabviewServer
    s << "\r\n";
    wxCharBuffer buf = s.ToUTF8();
    // Send the characters to the server
    if (m_socket)
    {

        if (m_socket->IsConnected())
        {
            if (printdebug)
                wxLogMessage("sending to server: %s", s);
            m_socket->Write(buf.data(), strlen(buf.data()));
        }
        else
        {
            wxLogMessage("cannot send data to servers");
        }
    }
    m_sendok = true;
}

/*******************************************************************************************************/
/************************     Log and Send Command Corvus Dialog       *********************************/
/*******************************************************************************************************/
LogandsendCmdCrvsDialog::LogandsendCmdCrvsDialog(wxWindow *parent)
    : LogandsendCmdCrvsDialog_fb(parent)
{
    // m_frame = (DOFrame *)parent;
    // m_crvsctrl = m_frame->GetCrvsControl();

    // static const int INTERVAL = 1747;                      // milliseconds
    // m_reloadtimer = new wxTimer(this, ID_TIMER_LOGRELOAD); // set owner and id in constructor
    // m_reloadtimer->Start(INTERVAL);

    // ReloadLogFromFile();
}
LogandsendCmdCrvsDialog::~LogandsendCmdCrvsDialog()
{
    // delete m_reloadtimer;
}

void LogandsendCmdCrvsDialog::ReloadLogFromFile()
{
    // m_textCtrlSrvLog->Clear();

    // m_crvsctrl->logger->Flush();
    // m_crvsctrl->logfile.flush();
    // wxFileInputStream input(wxT("Corvuslog.txt"));
    // if (!input.IsOk())
    //     wxLogMessage("Cannot open input file stream");
    // wxTextInputStream text(input, wxT("\x09"), wxConvUTF8);
    // while (input.IsOk() && !input.Eof())
    // {
    //     wxString line = text.ReadLine();
    //     (*m_textCtrlSrvLog) << line << '\n';
    // }
}

void LogandsendCmdCrvsDialog::OnLogandsendCmd(wxCommandEvent &event)
{
    // if (event.GetId() == ID_LOGSENDCMD_SENDCMD)
    // {
    //     wxString s = m_textCtrlSrvCmd->GetLineText(0);
    //     m_crvsctrl->CorvusSendCmd(s, true);
    // }
    // else if (event.GetId() == wxID_EXIT)
    // {
    //     Destroy();
    // }
}

void LogandsendCmdCrvsDialog::OnTimerLogFileReload(wxTimerEvent &event)
{
    ReloadLogFromFile();
}

/*******************************************************************************************************/
/************************     Connect From Corvus Server Dialog      ***********************************/
/*******************************************************************************************************/
ConnectCrvsDialog::ConnectCrvsDialog(wxWindow *parent)
    : ConnectCrvsDialog_fb(parent)
{
    // m_frame = (DOFrame *)parent;
    // m_crvsctrl = m_frame->GetCrvsControl();
    // m_sock = m_crvsctrl->GetSocket();
}

void ConnectCrvsDialog::OnConnectCrvsServer(wxCommandEvent &event)
{
    // wxString hostname = m_textCtrlServerIP->GetLineText(0);
    // wxString port = m_textCtrlServerPort->GetLineText(0);
    // m_crvsctrl->ConnectServer(hostname, port);

    // m_frame->UpdateStatusBar();

    // Destroy();
}

/*******************************************************************************************************/
/************************     Disconnect From Corvus Server Dialog       *******************************/
/*******************************************************************************************************/
DisconnectCrvsDialog::DisconnectCrvsDialog(wxWindow *parent)
    : DisconnectCrvsDialog_fb(parent)
{
    // m_frame = (DOFrame *)parent;
    // m_crvsctrl = m_frame->GetCrvsControl();
    // m_sock = m_crvsctrl->GetSocket();
}

void DisconnectCrvsDialog::OnDisconnectCrvsServer(wxCommandEvent &event)
{
    // if (m_sock->IsConnected())
    // {
    //     m_sock->Close();
    // }

    // if (m_sock->IsConnected())
    // {
    //     wxMessageBox("Cannot disconnect from server!!!", "Disconnection Error", wxICON_ERROR);
    //     wxLogMessage("Cannot disconnect from server.");
    // }
    // else
    // {
    //     wxMessageBox("Disconnected from server", "Connection Closed", wxICON_INFORMATION);
    //     wxLogMessage("Connection to server closed");
    // }
    // m_frame->UpdateStatusBar();

    // Destroy();
}
