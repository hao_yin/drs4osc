#include "VoltageScanDialog.h"
#include "DRSOscInc.h"

#include <wx/msgdlg.h>

#include <string>
#include <algorithm>

namespace run_control
{
    RCError::RCError(const std::string &msg) noexcept : std::runtime_error(msg) {}

    power_supply::rpc::CurrentState RunControl::Connect(const std::string &ip, int16_t port)
    {
        {
            wxCriticalSectionLocker guard(m_lock);
            m_ps = std::make_unique<power_supply::Controller>(ip, port);
            m_state = RCState::CONNECTED;
        }

        return this->GetPSState();
    }

    void RunControl::Disconnect()
    {
        wxCriticalSectionLocker guard(m_lock);
        try
        {
            m_state = RCState::DISCONNECTED;
            if (!m_ps.get())
                return void();

            auto state = m_ps->State();
            if ((state.state != "idle") || (state.state != "stopping"))
            {
                auto msg = std::stringstream();
                msg << "Current power supply state is still in `" << state.state << "'! If not in use, turn it off manually.";
                wxLogWarning(msg.str().c_str());
            }
        }
        catch (const power_supply::rpc::RPCError &err)
        {
            wxLogWarning(err.what());
        }
    }

    power_supply::rpc::CurrentState RunControl::GetPSState()
    {
        wxCriticalSectionLocker guard(m_lock);
        try
        {
            auto cur_state = m_ps->State();
            if (((m_state == RCState::RAMPING) 
                    || (m_state == RCState::CONFIGURE)) 
                    && (cur_state.state == "continuous"))
                m_state = RCState::RUNNING;
            return cur_state;
        }
        catch (const power_supply::rpc::RPCError &err)
        {
            m_state = RCState::FUCK_MSVC;
            throw RCError(err.what());
        }
        catch (const tcp_socket::TCPError &err)
        {
            throw RCError(err.what());
        }
    }

    void RunControl::Configure(const power_supply::rpc::Configure &cfg)
    {
        wxCriticalSectionLocker guard(m_lock);
        try
        {
            m_ps->Configure(cfg);
            m_state = RCState::CONFIGURE;
        }
        catch (const power_supply::rpc::RPCError &err)
        {
            m_state = RCState::FUCK_MSVC;
            throw RCError(err.what());
        }
    }

    void RunControl::ChangeVoltage(const power_supply::rpc::ChangeVoltage &cfg)
    {
        wxCriticalSectionLocker guard(m_lock);
        try
        {
            m_ps->ChangeVoltage(cfg);
            m_state = RCState::RAMPING;
        }
        catch (const power_supply::rpc::RPCError &err)
        {
            m_state = RCState::FUCK_MSVC;
            throw RCError(err.what());
        }
    }

    void RunControl::Stop()
    {
        wxCriticalSectionLocker guard(m_lock);
        try
        {
            m_ps->Stop();
            m_state = RCState::CONNECTED;
        }
        catch (const power_supply::rpc::RPCError &err)
        {
            m_state = RCState::FUCK_MSVC;
            throw RCError(err.what());
        }
    }
} // namespace run_control

VoltageScanDialog::VoltageScanDialog(wxWindow *parent, VSRunDependency &&external_dependencies) noexcept
    : VoltageScan_fb(parent)
{
    this->InitGUI();
    m_timer = new wxTimer(this);
    this->Connect(wxEVT_TIMER, wxTimerEventHandler(VoltageScanDialog::OnTimerUpdate), NULL, this);
    m_timer->Start(VoltageScanDialog::QUERY_RATE, wxTIMER_CONTINUOUS);
    m_dependencies = std::move(external_dependencies);
}

void VoltageScanDialog::OnVSConnect(wxCommandEvent &event)
{
    m_ctrl = std::make_unique<run_control::RunControl>();
    auto cur_state = power_supply::rpc::CurrentState();
    try
    {
        cur_state = m_ctrl->Connect(this->GetIP(), this->GetPort());
    }
    catch (const run_control::RCError &err)
    {
        wxLogWarning(err.what());
        if (m_ctrl.get())
            m_ctrl->Disconnect();
        return void();
    }

    this->UpdateConnected(cur_state);
    m_btnConnect->Enable(false);
    m_btnDisconnect->Enable(true);
    m_btnSave->Enable(true);

    auto msg = std::stringstream();
    msg << "Connected to the power supply over `" << this->GetIP() << ':' << this->GetPort() << "`!";
    wxLogInfo(msg.str().c_str());
}

void VoltageScanDialog::OnVSDisconnect(wxCommandEvent &event)
{
    if (m_ctrl.get())
        m_ctrl->Disconnect();

    this->UpdateDisconnected();
    m_btnConnect->Enable(true);
    m_btnDisconnect->Enable(false);
    m_btnSave->Enable(false);

    auto msg = std::stringstream();
    msg << "Power supply disconnected!";
    wxLogInfo(msg.str().c_str());
}

void VoltageScanDialog::UpdateConnected(const CurrentState &state) noexcept
{
    auto label = state.state;
    std::transform(std::begin(label), std::end(label), std::begin(label), std::toupper);
    m_lblState->SetLabel(wxString(label.c_str()));

    label = state.measurement_type;
    std::transform(std::begin(label), std::end(label), std::begin(label), std::toupper);
    m_lblType->SetLabel(wxString(label.c_str()));

    label = state.sample;
    std::transform(std::begin(label), std::end(label), std::begin(label), std::toupper);
    m_lblSample->SetLabel(wxString(label.c_str()));

    auto ss = std::stringstream();

    ss << state.source_voltage << " V";
    m_lblSource->SetLabel(ss.str().c_str());
    ss.str(std::string());

    ss << state.smu_current << " nA";
    m_lblSMUCurrent->SetLabel(ss.str().c_str());
    ss.str(std::string());

    ss << state.elm_current << " uA";
    m_lblELMCurrent->SetLabel(ss.str().c_str());
    ss.str(std::string());

    ss << state.lcr_capacity << " mF";
    m_lblLCRCapacity->SetLabel(ss.str().c_str());
    ss.str(std::string());

    ss << state.temperature << " degC";
    m_lblTemperature->SetLabel(ss.str().c_str());
}

void VoltageScanDialog::UpdateDisconnected() noexcept
{
    m_lblState->SetLabel("DISCONNECTED");
    m_lblType->SetLabel("UNKNOWN");
    m_lblSample->SetLabel("UNKNOWN");

    m_lblSource->SetLabel("-");
    m_lblSMUCurrent->SetLabel("-");
    m_lblELMCurrent->SetLabel("-");
    m_lblLCRCapacity->SetLabel("-");
    m_lblTemperature->SetLabel("-");
}

void VoltageScanDialog::OnVSSave(wxCommandEvent &event)
{
    if (!m_ctrl.get()) {
        const static auto msg = "Power supply ot connected!";
        wxMessageBox(msg, "WARNING - PS Connection",wxICON_ERROR);
        wxLogWarning(msg);
        return void();
    }
    m_ctrl->GetPSState();
    auto ctrl_state = m_ctrl->GetRCState();
    switch (ctrl_state)
    {
    case run_control::RCState::DISCONNECTED:
    case run_control::RCState::CONNECTED: {
        if (this->StartRun())
            m_btnSave->SetLabel("Close");
    } break;
    case run_control::RCState::FUCK_MSVC:
    case run_control::RCState::RAMPING:
    case run_control::RCState::CONFIGURE:
    case run_control::RCState::RUNNING: {
        auto reply = wxMessageBox(
            "Do you want to stop/abort the run?",
            "Comfirm", 
            wxYES_NO | wxCANCEL, this
        );

        if (reply == wxYES) {
            this->StopRun();
            {
                wxCriticalSectionLocker guard(**(m_dependencies.gui_lock));
                this->CloseOpenedFile();
            }
            wxLogInfo("Run Stopped");
            m_btnSave->SetLabel("Save");
        }
    } break;
    }
}

void VoltageScanDialog::SetExitCondition() const noexcept
{
    *m_dependencies.max_events = m_run_cfg.steps() * m_run_cfg.events_per_step;
    *m_dependencies.cur_events = 0;
}

void VoltageScanDialog::OpenCSV(float voltage) const
{
    *m_dependencies.file_dependency.file_type = CSV_FILE;
    int &fd = *(m_dependencies.file_dependency.file_descriptor);
    if (*m_dependencies.file_dependency.xml_writer)
        mxml_close_file(*m_dependencies.file_dependency.xml_writer);
    *m_dependencies.file_dependency.xml_writer = nullptr;

    if (fd)
        close(fd);
    fd = open(m_file_cfg.filename(voltage).c_str(), O_RDWR | O_CREAT | O_TRUNC, 0644);

    if (fd <= 0)
    {
        fd = 0;
        throw run_control::RCError("Can not open CSV file!");
    }
    m_dependencies.file_dependency.osci->SetEventSerial(1);
}

void VoltageScanDialog::OpenBinary(float voltage) const
{
    *m_dependencies.file_dependency.file_type = BINARY_FILE;
    int &fd = *(m_dependencies.file_dependency.file_descriptor);
    if (*m_dependencies.file_dependency.xml_writer)
        mxml_close_file(*m_dependencies.file_dependency.xml_writer);
    *m_dependencies.file_dependency.xml_writer = nullptr;
    if (fd)
        close(fd);

    fd = open(m_file_cfg.filename(voltage).c_str(), O_RDWR | O_CREAT | O_TRUNC, 0644);

    if (fd <= 0)
    {
        fd = 0;
        throw run_control::RCError("Can not open BINARY file!");
    }
    m_dependencies.file_dependency.osci->SetEventSerial(1);
}

auto VoltageScanDialog::OpenXML(float voltage) const -> void
{
    *m_dependencies.file_dependency.file_type = XML_FILE;
    auto &fd = *(m_dependencies.file_dependency.xml_writer);
    if (*m_dependencies.file_dependency.file_descriptor)
        close(*m_dependencies.file_dependency.file_descriptor);
    *m_dependencies.file_dependency.file_type = 0;
    if (fd)
        mxml_close_file(fd);

    fd = mxml_open_file(m_file_cfg.filename(voltage).c_str());

    if (fd <= 0)
    {
        fd = nullptr;
        throw run_control::RCError("Can not open XML file!");
    }
    m_dependencies.file_dependency.osci->SetEventSerial(1);
}

void VoltageScanDialog::OnVSClose(wxCommandEvent &event)
{
    this->Hide();
}

void VoltageScanDialog::InitGUI() noexcept
{
    this->UpdateDisconnected();
    m_btnConnect->Enable(true);
    m_btnDisconnect->Enable(false);
    m_btnSave->Enable(false);
}

VSRunConfiguration VoltageScanDialog::GetRunCfg()
{
    auto cfg = VSRunConfiguration{};
    cfg.step_size = m_dspbScanStepSize->GetValue();
    cfg.start_voltage = m_dspbInitV->GetValue();
    cfg.end_voltage = m_dspbFinalV->GetValue();
    cfg.events_per_step = m_dspbEventsPerStep->GetValue();

    auto msg = std::stringstream();
    if (cfg.step_size <= 0)
        msg << "\n\t- Invalid step size [V] value, expected a positive number!";

    if (cfg.start_voltage >= cfg.end_voltage)
        msg << "\n\t- `Start [V]` is greater or equal to `End [V]`!";

    if (!msg.str().empty())
        throw run_control::RCError(("Configuration Error:" + msg.str()).c_str());

    cfg.cur_step = 0;
    return cfg;
}

power_supply::rpc::Configure VoltageScanDialog::GetPSConfig()
{
    auto cfg = Configure{};
    cfg.begin_voltage = 0;
    cfg.end_voltage = m_dspbInitV->GetValue();
    cfg.compliance = m_dspbCompliance->GetValue();
    cfg.continuous = true;
    cfg.measurement_type = "iv";
    cfg.waiting_time = m_dspbWaitTime->GetValue();
    cfg.step_voltage = m_dspbStepSize->GetValue();
    cfg.reset = true;
    return cfg;
}

void VoltageScanDialog::OnTimerUpdate(wxTimerEvent &event) noexcept
{
    if (!m_ctrl.get() || (m_ctrl->GetRCState() == run_control::RCState::DISCONNECTED))
    {
        this->UpdateDisconnected();
        return void();
    }

    try
    {
        this->UpdateConnected(m_ctrl->GetPSState());
        auto msg = std::stringstream();
        msg << "Current State: " << static_cast<int>(m_ctrl->GetRCState()) << "!";
        wxLogInfo(msg.str().c_str());
    }
    catch (const run_control::RCError &err)
    {
        wxLogWarning(err.what());
    }
}

void VoltageScanDialog::StopOnOsciError(int osci_status) noexcept
{
    if (osci_status >= 0)
        return void();
    this->StopRun();    
}

bool VoltageScanDialog::SkipWhilePSInTransition() noexcept
{
    if (!m_ctrl.get())
        return false;

    auto rc_state = m_ctrl->GetRCState();
    if ((rc_state ==  run_control::RCState::CONNECTED) 
            || (rc_state ==  run_control::RCState::DISCONNECTED)
            || (rc_state == run_control::RCState::RUNNING))
        return false;

    m_ctrl->GetPSState();
    rc_state = m_ctrl->GetRCState();
    switch (rc_state)
    {
    case run_control::RCState::FUCK_MSVC:
    case run_control::RCState::CONFIGURE:
    case run_control::RCState::RAMPING:
        return true;
    default:
        return false;
    }
}

void VoltageScanDialog::ConfigureNextVoltage(int saved_events) noexcept
{
    if (!m_ctrl.get())
        return void();

    if (!m_run_cfg.events_per_step 
            || !saved_events
            || (saved_events % m_run_cfg.events_per_step))
        return void();
    // state update required:
    m_ctrl->GetPSState();
    auto ctrl_state = m_ctrl->GetRCState();
    
    switch (ctrl_state)
    {
    case run_control::RCState::RAMPING:
    case run_control::RCState::CONNECTED:
    case run_control::RCState::CONFIGURE:
    case run_control::RCState::DISCONNECTED:
    case run_control::RCState::FUCK_MSVC:
        return void();
    default:
        break;
    }

    auto cur_step = static_cast<int>(saved_events / m_run_cfg.events_per_step);
    auto next_voltage = m_run_cfg.next_voltage(cur_step);

    {
        auto msg = std::stringstream();
        msg << "Setting the next voltage to: `" << next_voltage << "`V! saved: " << saved_events << ", steps: " << m_run_cfg.events_per_step
            << ", cur_step: " << cur_step;
        wxLogInfo(msg.str().c_str());
    }

    auto cfg = power_supply::rpc::ChangeVoltage(next_voltage);
    static constexpr auto MAX_RETRIES = 3;
    auto i_ret = 0;
    for (; i_ret < MAX_RETRIES; ++i_ret)
    {
        try
        {
            m_ctrl->ChangeVoltage(cfg);
            if (m_file_cfg.postfix == ".csv")
                this->OpenCSV(next_voltage);
            else if (m_file_cfg.postfix == ".xml")
                this->OpenXML(next_voltage);
            else
                this->OpenBinary(next_voltage);
            return void(); // return if every thing is successfull
        }
        catch (const run_control::RCError &err)
        {
            wxLogWarning(err.what());
        }
    }

    if (i_ret == MAX_RETRIES)
    {
        auto msg = std::stringstream();
        msg << "Failed to change voltage! Please ramp down the power supply manually.";
        wxLogError(msg.str().c_str());
    }
}

bool VoltageScanDialog::StartRun() noexcept {
    if (*m_dependencies.file_dependency.file_descriptor || *m_dependencies.file_dependency.xml_writer)
    {
        wxMessageBox(
            wxT("Currently a run is ongoing, please wait until it has completed!"),
            wxT("ERROR - Run ongoing"),
            wxICON_ERROR);
        return false;
    }

    auto filename = wxFileSelector(
        wxT("Choose a file to write to"),
        wxEmptyString,
        wxEmptyString, wxT(".xml"),
        wxT("XML file (*.xml)|*.xml|Binary file (*.dat)|*.dat|Comma Separated File (*.csv)|*.csv"),
        wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

    if (!filename)
        return false;

    try
    {
        m_run_cfg = this->GetRunCfg();
    }
    catch (const run_control::RCError &err)
    {
        wxMessageBox(err.what(), "ERROR - Configuration", wxICON_ERROR);
        wxLogError(err.what());
        return false;
    }

    {
        auto file = filename.ToStdString();
        const auto pos = file.find_last_of(".");
        m_file_cfg.postfix = pos != std::string::npos ? file.substr(pos) : std::string();
        m_file_cfg.prefix = std::string(
            std::begin(file),
            pos != std::string::npos ? std::begin(file) + pos : std::end(file));

        try
        {
            wxCriticalSectionLocker guard(**(m_dependencies.gui_lock));
            if (m_file_cfg.postfix == ".csv")
                this->OpenCSV(m_run_cfg.start_voltage);
            else if (m_file_cfg.postfix == ".xml")
                this->OpenXML(m_run_cfg.start_voltage);
            else
                this->OpenBinary(m_run_cfg.start_voltage);
            this->SetExitCondition();
            m_ctrl->Configure(this->GetPSConfig());
            return true; // main ret if success.
        }
        catch (const run_control::RCError &err)
        {
            wxMessageBox(wxString(err.what()),
                         wxString("ERROR - Output Files"),
                         wxICON_ERROR);
            wxLogError(err.what());
        }
    }
    return false;
}

void VoltageScanDialog::CloseOpenedFile() const noexcept {
    if (*m_dependencies.file_dependency.xml_writer)
        mxml_close_file(*m_dependencies.file_dependency.xml_writer);
    *m_dependencies.file_dependency.xml_writer = nullptr;
    if (*m_dependencies.file_dependency.file_descriptor)
        close(*m_dependencies.file_dependency.file_descriptor);
    *m_dependencies.file_dependency.file_type = 0;
}

void VoltageScanDialog::StopRun() noexcept {
    static constexpr auto MAX_RETRIES = 3;
    auto i_ret = 0;
    for (; i_ret < MAX_RETRIES; ++i_ret)
    {
        try
        {
            auto cur_state = m_ctrl->GetPSState();
            if ((cur_state.state == "stop") || (cur_state.state == "stopping"))
                return void();
            break;
        }
        catch (const run_control::RCError &err)
        {
            wxLogWarning(err.what());
        }
    }

    if (i_ret >= MAX_RETRIES)
    {
        auto msg = std::stringstream();
        msg << "Failed to query power supply state on an Osci error! Please ramp down the power supply manually.";
        wxLogError(msg.str().c_str());
        return void();
    }

    for (i_ret = 0; i_ret < MAX_RETRIES; ++i_ret)
    {
        try
        {
            m_ctrl->Stop();
            return void();
        }
        catch (const run_control::RCError &err)
        {
            wxLogWarning(err.what());
        }
    }

    auto msg = std::stringstream();
    msg << "Failed to stop the power supply on an Osci error! Please ramp down the power supply manually.";
    wxLogError(msg.str().c_str());
    return void();
}   
