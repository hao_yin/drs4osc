/********************************************************************\

  Name:         drs4_corvus.cpp
  Created by:   Stefan Kupfer

  Contents:     Read out a DRS4 evaluation board with Corvus Control.
                Roughly based on drs4_exam (version 21308) by Stefan Ritt

  $Id: drs4_corvus.cpp 210101 2021-01-25 kupfer $

\********************************************************************/

#include <cmath>
#include <time.h>
#include <algorithm>

#ifdef _MSC_VER

#include <windows.h>

#elif defined(OS_LINUX)

#define O_BINARY 0

#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <errno.h>

#define DIR_SEPARATOR '/'
inline void Sleep(useconds_t x)
{
    usleep(x * 1000);
}
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "drs4_corvus.h"

static bool abs_compare(int a, int b)
{
    return (std::fabs(a) < std::fabs(b));
}

DRS4exam::~DRS4exam()
{
    delete drs;
}

bool DRS4exam::Init()
{
    /* do initial scan */
    drs = new DRS();

    /* show any found board(s) */
    for (i = 0; i < drs->GetNumberOfBoards(); i++)
    {
        b = drs->GetBoard(i);
        printf("Found DRS4 evaluation board, serial #%d, firmware revision %d\n",
               b->GetBoardSerialNumber(), b->GetFirmwareVersion());
    }

    /* exit if no board found */
    nBoards = drs->GetNumberOfBoards();
    if (nBoards == 0)
    {
        printf("No DRS4 evaluation board found\n");
        return false;
    }

    /* continue working with first board only */
    b = drs->GetBoard(0);

    /* initialize board */
    b->Init();

    /* set sampling frequency */
    b->SetFrequency(5, true);

    /* enable transparent mode needed for analog trigger */
    b->SetTranspMode(1);

    /* use following line to turn on the internal 100 MHz clock connected to all channels  */
    // b->EnableTcal(1);

    /* set input range to -0.5V ... +0.5V */
    b->SetInputRange(0);

    /* use following line to set range to 0..1V */
    // b->SetInputRange(0.5);

    return true;
}

/*
   Setting triggering to external Trigger Input 5V TTL
*/
void DRS4exam::SetExternalTrigger()
{
    /* use following lines to enable the external trigger */
    if (b->GetBoardType() == 8 || b->GetBoardType() == 9)
    {                                // Evaluaiton Board V4
        b->EnableTrigger(1, 0);      // enable hardware trigger
        b->SetTriggerSource(1 << 4); // set external trigger as source
    }
    else
    {                           // Evaluation Board V3
        b->EnableTrigger(1, 0); // lemo on, analog trigger off
    }
}

/*
   trigger level must be in interval [-0.5, 0.5]
*/
void DRS4exam::SetTriggerLvl(float triglevel)
{
    if (triglevel > -0.5 && triglevel < 0.5)
    {
        b->SetTriggerLevel(triglevel);
        m_triggerlevel = triglevel;
    }
}

/*
   Hardware trigger on channel
   if negedge true then triggering on negative edge
*/
void DRS4exam::SetTriggerChannel(int channel, bool negedge)
{
    // channel CH1 maps to 0, 1
    // channel CH2 maps to 1, 2
    channel--;
    m_triggerchannel = channel;
    if (channel >= 0 && channel <= 3)
    {
        /* use following lines to enable hardware trigger positive edge */
        if (b->GetBoardType() >= 8)
        {                                      // Evaluaiton Board V4&5
            b->EnableTrigger(1, 0);            // enable hardware trigger
            b->SetTriggerSource(1 << channel); // set CH1 as source
        }
        else if (b->GetBoardType() == 7)
        {                           // Evaluation Board V3
            b->EnableTrigger(0, 1); // lemo off, analog trigger on
            b->SetTriggerSource(0); // use CH1 as source
        }
        b->SetTriggerPolarity(negedge); // if false triggering on positive edge

        b->SetTriggerDelayNs(0); // zero ns trigger delay
    }
    // external trigger requires larger delay
    else if (channel == 4)
    {
        SetExternalTrigger();
        b->SetTriggerDelayNs(170); // zero ns trigger delay
    }
}

int DRS4exam::GetTriggerSource()
{
    return b->GetTriggerSource();
}

void DRS4exam::SetMeasureChannel(int channel)
{
    channel--;
    m_measurechannel = channel;
}
/*
   connect internal 100 MHz clock signal to all channels
   if flag is true
   MUX switch: 100MHzSignal | Channel input
*/
void DRS4exam::SetTcal(bool flag)
{
    b->EnableTcal(flag ? 1 : 0);
}

/*
    Start measurement
    enable trigger, start domino ring sampler, readout data
*/
int DRS4exam::StartMeasurement(bool printout)
{
    b->EnableTrigger(1, 0);

    /* start board (activate domino wave) */
    b->StartDomino();

    /* wait for trigger */
#ifdef __linux__
    printf("Waiting for trigger...");
    fflush(stdout);
#endif

    int triggered = 0;
    clock_t tstart = clock();
    while (b->IsBusy())
    {
        if (triggered == 0 && (clock() - tstart) / (double)CLOCKS_PER_SEC > 1.0)
        {
            triggered = 1;
            // apply software trigger if triggering was not succesfull
            b->EnableTrigger(0, 0);
            b->SoftTrigger();
            tstart = clock();
#ifdef __linux__
            printf("Cannot hard trigger\n");
            fflush(stdout);
#endif
        }
        if (triggered == 1 && (clock() - tstart) / (double)CLOCKS_PER_SEC > .5)
        {
#ifdef __linux__
            printf("Cannot soft trigger\n");
            fflush(stdout);
#endif
            triggered = 2;
            return triggered;
        }
        Sleep(10);
    }

    /* read all waveforms */
    int chip = 0;
    b->TransferWaves(0, 8);
    m_triggercell = b->GetStopCell(chip);
    m_writeSR = b->GetStopWSR(chip);

    /* read time (X) array of second channel in ns
    Note: On the evaluation board input #1 is connected to channel 0 and 1 of
    the DRS chip, input #2 is connected to channel 2 and 3 and so on. So to
    get the input #2 we have to read DRS channel #2, not #1. */
    for (int i = 0; i < 4; i++)
    {
        /* read time (X) array of i channel in ns */
        b->GetTime(0, 2 * i, b->GetTriggerCell(0), m_time_array[i], m_tcalon, m_rotated);

        /* decode waveform (Y) array of i channel in mV */
        b->GetWave(0, 2 * i, m_wave_array[i], m_calibrated, m_triggercell, m_writeSR, !m_rotated, 0, m_calibrated2);
    }

    // first two are noisy
    for (int j = 0; j < 4; j++)
    {
        m_wave_array[j][1] = 2 * m_wave_array[j][2] - m_wave_array[j][3];
        m_wave_array[j][0] = 2 * m_wave_array[j][1] - m_wave_array[j][2];
    }

    if (m_removespikes)
        RemoveSpikes();

    if (printout)
    {
        /* open file to save waveforms */
        FILE *f = fopen("wavedata_raw.dat", "w");
        if (f == NULL)
        {
            perror("ERROR: Cannot open file \"wavedata_raw.dat\"");
            return 1;
        }

        /* Save waveform: X=time_array[i], Yn=wave_array[n][i] */
        for (int j = 0; j < 1024; j++)
            fprintf(f, "%7.3f %7.1f %7.3f %7.1f %7.3f %7.1f %7.3f %7.1f \n", m_time_array[0][j],
                    m_wave_array[0][j],
                    m_time_array[1][j],
                    m_wave_array[1][j],
                    m_time_array[2][j],
                    m_wave_array[2][j],
                    m_time_array[3][j],
                    m_wave_array[3][j]);

        fclose(f);
    }
    return triggered;
}

/*
    Average every samplepoint nMeasurements times
    if printout only the first measurement is written out
*/
avrgmeas DRS4exam::StartAveragerMeas(int nMeasurements, bool printout)
{
    avrgmeas avrgmeas;
    int er;

    // dummy measurements
    for (int i = 0; i < 2; i++)
    {
        StartMeasurement(false);
    }

    float *ppkpkval = new float[nMeasurements];

    for (int i = 0; i < nMeasurements; i++)
    {
        er = StartMeasurement(printout);
        if (printout)
            printout = false; // printout the first wave

        if (er == 0)
        {
            // calculating peak peak for entire waveform (avrg2)
            float *wave_max = std::max_element(m_wave_array[m_measurechannel], m_wave_array[m_measurechannel] + 1024);
            float *wave_min = std::min_element(m_wave_array[m_measurechannel], m_wave_array[m_measurechannel] + 1024);
            float ppkval = *wave_max - *wave_min;
            if (fabs(*wave_min) > fabs(*wave_max))
                ppkval *= -1;
            ppkpkval[i] = ppkval;

            // add up every sample point for averaging
            for (int j = 0; j < 1024; j++)
            {
                if (i == 0)
                {
                    m_time_avg_array[j] = m_time_array[m_measurechannel][j];
                    m_wave_avg_array[j] = m_wave_array[m_measurechannel][j];
                    m_time_std_array[j] = pow((double)m_time_array[m_measurechannel][j], 2);
                    m_wave_std_array[j] = pow((double)m_wave_array[m_measurechannel][j], 2);
                }
                else
                {
                    m_time_avg_array[j] += m_time_array[m_measurechannel][j];
                    m_wave_avg_array[j] += m_wave_array[m_measurechannel][j];
                    m_time_std_array[j] += pow((double)m_time_array[m_measurechannel][j], 2);
                    m_wave_std_array[j] += pow((double)m_wave_array[m_measurechannel][j], 2);
                }
            }
        }
        else
        {
            avrgmeas.error = er;
            return avrgmeas;
        }
    }
    for (int i = 0; i < 1024; i++)
    {
        m_time_avg_array[i] /= (float)nMeasurements;
        m_wave_avg_array[i] /= (float)nMeasurements;
        m_time_std_array[i] /= (double)nMeasurements;
        m_wave_std_array[i] /= (double)nMeasurements;
        // std deviation in % to the average value at index i
        m_time_std_array[i] = sqrt(m_time_std_array[i] - pow(m_time_avg_array[i], 2)); // / (double)m_time_avg_array[i];
        m_wave_std_array[i] = sqrt(m_wave_std_array[i] - pow(m_wave_avg_array[i], 2)); // / (double)m_wave_avg_array[i];
    }

    float *wave_max = std::max_element(m_wave_avg_array, m_wave_avg_array + 1024);
    float *wave_min = std::min_element(m_wave_avg_array, m_wave_avg_array + 1024);

    double *ptime_std_max = std::max_element(m_time_std_array, m_time_std_array + 1024,
                                             abs_compare);
    double *pwave_std_max = std::max_element(m_wave_std_array, m_wave_std_array + 1024,
                                             abs_compare);

    float pkpkstd = 0, pkpkavrg = 0;
    for (int i = 0; i < nMeasurements; i++)
    {
        pkpkavrg += ppkpkval[i];
        pkpkstd += pow(ppkpkval[i], 2);
    }
    pkpkavrg /= (float)nMeasurements;
    pkpkstd /= (float)nMeasurements;
    pkpkstd = sqrt(pkpkstd - pkpkavrg * pkpkavrg);

    avrgmeas.error = er;
    avrgmeas.wavemax = *wave_max;
    avrgmeas.wavemin = *wave_min;
    avrgmeas.time_std = *ptime_std_max;
    avrgmeas.wave_std = *pwave_std_max;
    avrgmeas.ppkavrg2 = pkpkavrg;
    avrgmeas.ppkstd2 = pkpkstd;

    printf("\nMax std deviation in time: %f\n# Max std deviation in voltage: %f\n",
           avrgmeas.time_std, avrgmeas.wave_std);

    return avrgmeas;
}

/*
    start peak to peak measurements nMeasurements times
    if printout wavedata_raw.dat and wavedat_avrg.dat are written
*/
int DRS4exam::StartPeakPeakMeas(int nMeasurements, bool printout)
{
    avrgmeas avrgmeas = StartAveragerMeas(nMeasurements, printout);
    if (avrgmeas.error != 0)
        return avrgmeas.error;

    // calculate peak-peakvalue and set right sign
    m_ppkval = avrgmeas.wavemax - avrgmeas.wavemin;
    m_ppkvalstd = avrgmeas.wave_std;
    m_timestd = avrgmeas.time_std;
    m_ppkvalstd = avrgmeas.wave_std;
    m_ppkavrg2 = avrgmeas.ppkavrg2;
    m_ppkstd2 = avrgmeas.ppkstd2;

    if (fabs(avrgmeas.wavemin) > fabs(avrgmeas.wavemax))
        m_ppkval *= -1;

    if (printout)
    {
        /* open file to save waveforms */
        FILE *f = fopen("wavedata_avrg.dat", "w");
        if (f == NULL)
        {
            perror("ERROR: Cannot open file \"wavedata_avrg.dat\"");
            return 1;
        }

        /* Save waveform: X=time_array[i], Yn=wave_array[n][i] */
        fprintf(f, "# Averaged time and wave for ch: %d \n# t[ns]  upp[mV]  std time[ns]   std upp[mV]\n", m_measurechannel);
        fprintf(f, "# triggerchannel %d  triggercell %d\n", m_triggerchannel, b->GetTriggerCell(0));
        fprintf(f, "# Voltage peak peak: %.3f\n", m_ppkval);
        fprintf(f, "# Max std deviation in time: %f ns\n#Max std deviation in voltage: %f mV\n", avrgmeas.time_std, avrgmeas.wave_std);
        fprintf(f, "# Avrg on peak itself: %f mV   std deviation of avrg2 %f mV\n", m_ppkavrg2, m_ppkstd2);
        for (int j = 0; j < 1024; j++)
            fprintf(f, "%+10.3f %+10.3f %+10.3f %+10.3f \n", m_time_avg_array[j], m_wave_avg_array[j],
                    m_time_std_array[j], m_wave_std_array[j]);

        fclose(f);
    }
    return 0;
}

/*
    Simple Highpass filter to find index and remove spikes
    This will look for spikes with min difference of 24mV and removes them
*/
void DRS4exam::RemoveSpikes()
{
    float dyl, dyr;
    float maxdev = 24.f;
    int il, ir;
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 1024; j += 8)
        {
            // search from the left end of cell
            for (int k = 0; k < 8 - 1; k++)
            {
                il = k;
                float y = m_wave_array[i][j + k];
                float yp1 = m_wave_array[i][j + k + 1];
                dyl = yp1 - y;
                if (fabs(dyl) > maxdev)
                {
                    break; // spike detected, remeber index and continue from the right
                }
            }
            if (il == 6)
                continue; // no spike detected
            // now search from the right end of cell
            for (int k = 7; k > 0; k--)
            {
                ir = k;
                float y = m_wave_array[i][j + k];
                float ym1 = m_wave_array[i][j + k - 1];
                dyr = y - ym1;
                if (fabs(dyr) > maxdev)
                {
                    break;
                }
            }
            if (ir < il)
            {
                printf("Remove spikes: Something went wrong ");
                continue;
            }
            if (ir - il < 2)
            {
                printf("Remove spikes: Removin Peak ");
                if (ir == il)
                {
                    if (il > 0)
                        m_wave_array[i][il] = m_wave_array[i][il - 1];
                    else
                        m_wave_array[i][ir] = m_wave_array[i][ir + 1];
                }
                if (ir - il == 1)
                {
                    if (il > 0)
                    {
                        m_wave_array[i][il] = m_wave_array[i][il - 1];
                        m_wave_array[i][il + 1] = m_wave_array[i][il - 1];
                    }
                    else
                    {
                        m_wave_array[i][ir] = m_wave_array[i][ir + 1];
                        m_wave_array[i][ir - 1] = m_wave_array[i][il + 1];
                    }
                }
            }
        }
    }
}
