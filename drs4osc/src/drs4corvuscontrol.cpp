/***************************************************************************
    Name:         drs4corvuscontrol.cpp
    Created by:   Stefan Kupfer

    Contents:     GUI for the DRS4 readout with Corvus Control program

    $Id: drs4covurscontrol.cpp 210101 2021-01-25 kupfer $
***************************************************************************/

// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
#include "wx/socket.h"
#include "wx/url.h"
#include "wx/sstream.h"
#include "wx/thread.h"
#include "wx/timer.h"
#include "wx/tokenzr.h"
#include "wx/utils.h"
#include "wx/stopwatch.h"

#include <memory>

#if defined(_MSC_VER)
#include <windows.h>
#endif

#include "mathplot.h"

#include "drs4corvusforms.h"
#include "drs4_corvus.h"

#define TOLMOVE 10         // tolerance in movement in nm
#define TIMEASKPOS 2000    // time repetition for asking position from server in ms
#define TIMEASKPOSFAST 500 // fast asking position from server in ms for scan mode
#define TIMEREPLOT 300     // when showplot, replot after x ms

WX_DEFINE_ARRAY_PTR(wxThread *, wxArrayThread);

class MyApp : public wxApp
{
public:
    MyApp();
    virtual ~MyApp(){};
    virtual bool OnInit();

    wxCriticalSection m_critsect;
    wxArrayThread m_threads;

    // semaphore used to wait for the threads to exit, see MyFrame::OnQuit()
    wxSemaphore m_semAllDone;

    // indicates that we're shutting down and all threads should exit
    bool m_shuttingDown;
    bool m_closethread;
};

class MyThread;
class MyPlotFrame;

class MySIN : public mpFX
{
    double m_freq, m_amp;

public:
    MySIN(double freq, double amp) : mpFX(wxT("f(x) = SIN(x)"), mpALIGN_LEFT)
    {
        m_freq = freq;
        m_amp = amp;
        m_drawOutsideMargins = false;
    }
    virtual double GetY(double x) { return m_amp * sin(x / 6.283185 / m_freq); }
    virtual double GetMinY() { return -m_amp; }
    virtual double GetMaxY() { return m_amp; }
};

class MyFrame : public frameMain
{
public:
    MyFrame();
    ~MyFrame();
    // menu entries
    void OnHello(wxCommandEvent &event);
    void OnExit(wxCommandEvent &event);
    void OnAbout(wxCommandEvent &event);
    void OnQuit(wxCommandEvent &event);
    void OnShowPlot(wxCommandEvent &event);

    // buttons and checkboxes
    void ServerConnect();
    void ServerCloseCon();
    void ServerConCloseCheck(wxCommandEvent &event);
    void OnSocketEvent(wxSocketEvent &event);
    void CorvusSendbtn(wxCommandEvent &event);
    void OnSetTrigLvlbtn(wxCommandEvent &event);
    void OnLoadPostoStartbtn(wxCommandEvent &event);
    void ScanAxis(wxCommandEvent &event);
    void MoveStart(wxCommandEvent &event);
    void OnApply100MhzSig(wxCommandEvent &event);
    void OnDRSGetMeasure(wxCommandEvent &event); // measure button is pressed
    void OnComboSetMeasureChannel(wxCommandEvent &event);
    void OnComboSetTriggerChannel(wxCommandEvent &event);
    void OnComboSetNumAvrgMeas(wxCommandEvent &event);
    void OnRemoveSpikes(wxCommandEvent &event);

    // thread events
    void OnWorkerEvent(wxThreadEvent &event);
    void OnLogEvent(wxThreadEvent &event);
    void OnThreadStart(wxThreadEvent &);
    void OnThreadCompletion(wxThreadEvent &);
    void OnThreadStartMeas(wxThreadEvent &event);

    void OnTimer(wxTimerEvent &event);

    // void OnChildDestroy(wxWindowDestroyEvent& event);

    void CorvusSendCmd(wxString, bool printdebug = false);
    void GetActualPos();
    bool CheckBounds(int axisnumber);
    bool CreateThread();
    void StartMeas(bool printdebug);
    void UpdateStatusBar();
    void GetScanPositions(long xyzmoveloc[3]);
    void MoveAbsolute(long xyzmoveloc[3]);
    int GetNumMeasurements() { return m_numavrgmeas; };

    bool m_destroy = false;
    MyThread *m_scanthread;
    wxCriticalSection m_pThreadCS; // protects the m_scanthread pointer

    bool m_sockconnected = false;
    long m_posStart[3], m_posEnd[3];
    float m_stepsize[3];
    long m_xyzpos[3];
    double m_fxyzpos[3];
    bool m_checkBoxa[3] = {false, false, false};
    int m_status; // status of movement, from Server
    int m_drs4status;
    bool m_measongoing = false;
    DRS4exam *m_drs4exam;

private:
    wxSocketClient *m_sock;
    wxTimer *m_timer;

    bool m_getpos = false; // if true print into position fields
    bool m_sendok = true;  // lock for socket write
    bool m_scanongoing = false;
    bool m_deletethread = false;
    int m_numavrgmeas = 16;

    bool m_plotshow = false;
    MyPlotFrame *m_plotframe;

    // any class wishing to process wxWidgets events must use this macro
    wxDECLARE_EVENT_TABLE();
};

class MyThread : public wxThread
{

    bool m_exitthread = false;

public:
    MyThread(MyFrame *frame) : wxThread(wxTHREAD_DETACHED), m_frame(frame) {}

    virtual ~MyThread();

    void MoveRelative(float relval, int index);
    void MoveAbsolute(long xyzmoveloc[3]);
    bool WaitForPosition(long xyzpos[3]);
    int GetPosition(int axis);

protected:
    // thread execution starts here
    virtual void *Entry();

    MyFrame *m_frame;
};

class MyPlotFrame : public framePlot
{
public:
    mpWindow *m_plot;
    MyPlotFrame(MyFrame *frame);
    ~MyPlotFrame();
    void OnClose(wxCommandEvent &event) { Destroy(); };
    void OnTimer(wxTimerEvent &event);

    void Replot();

private:
    int axesPos[2];
    bool ticks;
    MyFrame *m_frame;
    mpInfoCoords *nfo; // mpInfoLayer* nfo;
    mpFXYVector *m_vectorLayer;
    wxTimer *m_timer;

    wxDECLARE_EVENT_TABLE();
};

enum
{
    ID_Hello = wxID_HIGHEST + 1,
    SOCKET_ID,
    TIMER_ID,
    PLOTTIMER_ID,
    PLOTSHOW_ID,
    ID_WORKER_THREAD,
    ID_LOG_THREAD,
    ID_FINISH_THREAD,
    ID_START_THREAD,
    ID_STARTMEAS_THREAD
};

// --------------------------------------------------------------------------
// event tables and other macros for wxWidgets
// --------------------------------------------------------------------------

wxBEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_MENU(wxID_ABOUT, MyFrame::OnAbout)
        EVT_MENU(wxID_EXIT, MyFrame::OnExit)
            EVT_TIMER(TIMER_ID, MyFrame::OnTimer)
                EVT_SOCKET(SOCKET_ID, MyFrame::OnSocketEvent)
                    EVT_THREAD(ID_WORKER_THREAD, MyFrame::OnWorkerEvent)
                        EVT_THREAD(ID_LOG_THREAD, MyFrame::OnLogEvent)
                            EVT_THREAD(ID_START_THREAD, MyFrame::OnThreadStart)
                                EVT_THREAD(ID_FINISH_THREAD, MyFrame::OnThreadCompletion)
                                    EVT_THREAD(ID_STARTMEAS_THREAD, MyFrame::OnThreadStartMeas)
                                        wxEND_EVENT_TABLE()

                                            wxBEGIN_EVENT_TABLE(MyPlotFrame, wxFrame)
                                                EVT_TIMER(PLOTTIMER_ID, MyPlotFrame::OnTimer)
                                                    wxEND_EVENT_TABLE()

                                                        wxIMPLEMENT_APP(MyApp);

MyApp::MyApp()
{
    m_shuttingDown = false;
    m_closethread = false;
}
bool MyApp::OnInit()
{
    MyFrame *frame = new MyFrame();
    frame->Show(true);
    return true;
}

MyFrame::MyFrame() : frameMain(NULL)
{
    //// Make a textctrl for logging
    wxLog::SetActiveTarget(new wxLogTextCtrl(m_textCtrlSrvLog));

    // Create the socket
    m_sock = new wxSocketClient();

    // Setup the event handler and subscribe to most events
    m_sock->SetEventHandler(*this, SOCKET_ID);
    m_sock->SetNotify(wxSOCKET_CONNECTION_FLAG |
                      wxSOCKET_INPUT_FLAG |
                      wxSOCKET_LOST_FLAG);
    m_sock->Notify(true); // wxwidgets change color button

    m_timer = new wxTimer(this, TIMER_ID);
    m_timer->SetOwner(this, TIMER_ID);

    m_drs4exam = new DRS4exam();

    if (m_drs4exam->Init())
    {
        m_drs4status = 1;
        wxLogMessage("Init of DRS4 board successsfull");
        m_textCtrlDrs4Status->Clear();
        *m_textCtrlDrs4Status << "Ok";
    }
    else
    {
        m_drs4status = 0;
        wxLogMessage("Init of DRS4 board FAILED");
        m_textCtrlDrs4Status->Clear();
        *m_textCtrlDrs4Status << "False";
    }

    // setting up Trigger
    if (m_drs4status)
    {
        int triggerchannel = 5; // setting to External Trigger
        m_drs4exam->SetTriggerChannel(triggerchannel);
        m_cmboTriggerChannel->SetSelection(triggerchannel - 1);
        if (triggerchannel == 5)
        {
            wxLogMessage("External Trigger set");

            m_textCtrlDRS4setTrigVal->Enable(false);
            m_btnSetTriggerLvl->Enable(false);
        }
        else if (triggerchannel > 0 && triggerchannel < 5)
        {
            float triggerval = 0.01f;
            m_drs4exam->SetTriggerLvl(triggerval);
            m_textCtrlDRS4setTrigVal->Clear();
            *m_textCtrlDRS4setTrigVal << (int)(triggerval * 1000);
            wxLogMessage("Trigger level set to: %f", triggerval);
        }
        else
        {
            wxLogMessage("!!!!!No inital valid triggerchannel, check code");
        }

        m_cmboTriggerChannel->Enable(true);
        m_cmboMeasureChannel->Enable(true);
        m_drs4exam->SetMeasureChannel(4);
        m_cmboNumAvrgMeas->Enable(true);
        m_chkboxRemoveSpikes->Enable(true);
    }
    else
    {
        m_textCtrlDRS4setTrigVal->Clear();
        *m_textCtrlDRS4setTrigVal << "Cannot set";

        m_cmboMeasureChannel->Enable(false);
        m_cmboTriggerChannel->Enable(false);
        m_cmboNumAvrgMeas->Enable(false);
        m_chkboxRemoveSpikes->Enable(false);
        m_chkboxApply100Mhzsig->Enable(false);
        m_textCtrlDRS4setTrigVal->Enable(false);
        m_btnSetTriggerLvl->Enable(false);
    }

    UpdateStatusBar();
}

MyFrame::~MyFrame()
{
    delete wxLog::SetActiveTarget(NULL);
    // No delayed deletion here, as the frame is dying anyway
    delete m_sock;
    delete m_drs4exam;
    delete m_timer;
}

void MyFrame::OnTimer(wxTimerEvent &event)
{
    GetActualPos();
}

void MyFrame::OnExit(wxCommandEvent &event)
{
    Close(true);
}

void MyFrame::OnAbout(wxCommandEvent &event)
{
    wxMessageBox("This is a testing program\nfor DRS4 sofware with corvus control",
                 "About DRS4 Corvus Connect, S.K.", wxOK | wxICON_INFORMATION);
}

void MyFrame::OnQuit(wxCommandEvent &WXUNUSED(event))
{
    Close(true);
}

void MyFrame::OnShowPlot(wxCommandEvent &event)
{
    wxWindow *w = FindWindowById(PLOTSHOW_ID);
    if (m_plotframe == NULL || w == NULL)
    {
        m_plotframe = new MyPlotFrame(this);
        m_plotframe->Show(true);
        m_plotframe->SetId(PLOTSHOW_ID);
    }
}

void MyFrame::OnSetTrigLvlbtn(wxCommandEvent &event)
{
    wxString s = m_textCtrlDRS4setTrigVal->GetValue();
    if (s.IsNumber())
    {
        if (m_drs4status)
        {
            float val = (float)wxAtoi(s) / 1000.;
            m_drs4exam->SetTriggerLvl(val);
            wxLogMessage(wxT("Trigger level set to %fV"), val);
            wxLogMessage("Trigger source set to %d", m_drs4exam->GetTriggerSource());
        }
    }
}

void MyFrame::OnDRSGetMeasure(wxCommandEvent &event)
{
    wxLogMessage("writing unaveraged data to dataraw.dat");
    wxLogMessage("writing averaged data to data.dat");
    StartMeas(true);
}

void MyFrame::OnComboSetNumAvrgMeas(wxCommandEvent &event)
{
    m_numavrgmeas = pow(2, m_cmboNumAvrgMeas->GetSelection());
    if (m_numavrgmeas < 0 || m_numavrgmeas > 1024)
        m_numavrgmeas = 16;
    wxLogMessage("Setting number of Averaging Samples to %d", m_numavrgmeas);
}

void MyFrame::OnLoadPostoStartbtn(wxCommandEvent &event)
{
    wxTextCtrl *textctrlposfrom_a[3] = {m_textCtrlXfrom, m_textCtrlYfrom, m_textCtrlZfrom};
    wxTextCtrl *textctrlposto_a[3] = {m_textCtrlXto, m_textCtrlYto, m_textCtrlZto};

    for (int i = 0; i < 3; i++)
    {
        textctrlposfrom_a[i]->Clear();
        *textctrlposfrom_a[i] << m_xyzpos[i];
        textctrlposto_a[i]->Clear();
        *textctrlposto_a[i] << m_xyzpos[i];
    }
    wxLogMessage("Loaded actual position into start and end fields");
}

void MyFrame::OnApply100MhzSig(wxCommandEvent &event)
{
    bool flag = m_chkboxApply100Mhzsig->IsChecked();
    m_drs4exam->SetTcal(flag);
    wxLogMessage("Apply 100Mhz to all channels: %s", flag ? "true" : "false");
}

void MyFrame::OnComboSetMeasureChannel(wxCommandEvent &event)
{
    int measurechnl = m_cmboMeasureChannel->GetSelection() + 1;
    wxLogMessage("Maeasure Channel %d selected", measurechnl);
    m_drs4exam->SetMeasureChannel(measurechnl);
}

void MyFrame::OnComboSetTriggerChannel(wxCommandEvent &event)
{
    int triggerchnl = m_cmboTriggerChannel->GetSelection() + 1;
    if (triggerchnl <= 4)
    {
        wxLogMessage("Trigger Channel %d selected", triggerchnl);
        m_textCtrlDRS4setTrigVal->Enable(true);
        m_btnSetTriggerLvl->Enable(true);
        m_textCtrlDRS4setTrigVal->Clear();
        *m_textCtrlDRS4setTrigVal << (int)(m_drs4exam->GetTriggerLevel() * 1000);
    }
    else if (triggerchnl == 5)
    {
        wxLogMessage("External TTL Trigger ");
        m_textCtrlDRS4setTrigVal->Enable(false);
        m_btnSetTriggerLvl->Enable(false);
    }
    m_drs4exam->SetTriggerChannel(triggerchnl);
    wxLogMessage("Trigger source set to %d", m_drs4exam->GetTriggerSource());
}

void MyFrame::OnRemoveSpikes(wxCommandEvent &event)
{
    bool flag = m_chkboxRemoveSpikes->IsChecked();
    m_drs4exam->SetRemoveSpikes(flag);
    wxLogMessage("Apply remove spikes: %s", flag ? "true" : "false");
}

#define MAXAXVAL 300000L

bool MyFrame::CheckBounds(int axisnumber)
{
    long *arraytocheck[2] = {m_posStart, m_posEnd};

    for (int i = 0; i < 2; i++)
    {
        if (*(arraytocheck[i] + axisnumber) < 0)
        {
            wxLogMessage("smaller than zero detected");
            return false;
        }
        else if (*(arraytocheck[i] + axisnumber) > MAXAXVAL)
        {
            wxLogMessage("greater than % detected", MAXAXVAL);
            return false;
        }
    }
    if (m_stepsize[axisnumber] > 1000.f)
        return false;
    return true;
}

void MyFrame::GetScanPositions(long xyzmoveloc[3])
{
    bool checkok = false;
    wxString loc;

    long textfields_start[] = {wxAtol(m_textCtrlXfrom->GetLineText(0)),
                               wxAtol(m_textCtrlYfrom->GetLineText(0)),
                               wxAtol(m_textCtrlZfrom->GetLineText(0))};

    long textfields_end[] = {wxAtol(m_textCtrlXto->GetLineText(0)),
                             wxAtol(m_textCtrlYto->GetLineText(0)),
                             wxAtol(m_textCtrlZto->GetLineText(0))};

    // long textfields_step[]  = { wxAtol(m_textCtrlXstepsize->GetLineText(0)),
    //                             wxAtol(m_textCtrlYstepsize->GetLineText(0)),
    //                             wxAtol(m_textCtrlZstepsize->GetLineText(0))  };

    float textfields_step[] = {(float)wxAtof(m_textCtrlXstepsize->GetLineText(0)),
                               (float)wxAtof(m_textCtrlYstepsize->GetLineText(0)),
                               (float)wxAtof(m_textCtrlZstepsize->GetLineText(0))};

    int checkbox[3] = {m_checkBoxXuse->GetValue(), m_checkBoxYuse->GetValue(),
                       m_checkBoxZuse->GetValue()};

    for (int i = 0; i < 3; i++)
    {
        xyzmoveloc[i] = m_xyzpos[i];

        if (checkbox[i])
        {
            m_checkBoxa[i] = true;
            m_posStart[i] = textfields_start[i];
            m_posEnd[i] = textfields_end[i];
            m_stepsize[i] = textfields_step[i];
            checkok = CheckBounds(i);
            if (checkok)
            {
                // moving to start position
                xyzmoveloc[i] = m_posStart[i];
            }
            else
            {
                wxLogMessage(wxT("axis %ld: out of bounds"), i);
            }
        }
        else
        {
            m_checkBoxa[i] = false;
            m_posStart[i] = m_xyzpos[i];
            m_posEnd[i] = m_xyzpos[i];
            m_stepsize[i] = 1.f;
        }
    }
}

void MyFrame::MoveStart(wxCommandEvent &WXUNUSED(event))
{
    long xyzmoveloc[3];
    GetScanPositions(xyzmoveloc);
    MoveAbsolute(xyzmoveloc);
    wxLogMessage(wxT("Moving to start Position:\n\t%ld %ld %ld"),
                 xyzmoveloc[0],
                 xyzmoveloc[1],
                 xyzmoveloc[2]);
}

/*
    if scan axis button was pressed the scan thread is starting
    returns true if thread was created and started running
*/
bool MyFrame::CreateThread()
{
    m_scanthread = new MyThread(this);

    if (m_scanthread->Create() != wxTHREAD_NO_ERROR)
    {
        wxLogMessage(wxT("Can't create thread!"));
        return false;
    }
    {
        wxCriticalSectionLocker enter(wxGetApp().m_critsect);
        wxGetApp().m_threads.Add(m_scanthread);
    }
    if (m_scanthread->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogMessage(wxT("Can't start thread!"));
        delete m_scanthread;
        m_scanthread = NULL;
        return false;
    }
    wxMilliSleep(300);
    if (m_scanthread)
        wxLogMessage(wxT("Type of thread: %s"), m_scanthread->IsDetached() ? "detached" : "joinable");
    return true;
}

/*
    Scan axis button pressed
    checks if already is running and changes to stop axis (stop scan thread) button
*/
void MyFrame::ScanAxis(wxCommandEvent &event)
{
    // wxThread* toDelete = NULL;
    bool bstartthread = true;

    // StartStop scan thread
    {
        wxCriticalSectionLocker enter(m_pThreadCS);
        if (m_scanthread)
        {
            m_deletethread = false;
            bstartthread = false;
            wxLogMessage("Start deleting scan thread");
#ifdef __linux__
            printf("starting deleting scan\n");
            fflush(stdout);
#endif
            /*	{
                    wxCriticalSectionLocker locker(wxGetApp().m_critsect);
                    wxGetApp().m_closethread = true;
                }*/
        }
    } // exit guarded section so that the thread can destroy itself

    if (bstartthread)
    {
        wxLogMessage("Moving to start and\n\tStart creating scan thread");
        MoveStart(event);
        CreateThread();
    }
    else
    {
        if (!m_deletethread)
        {
            m_deletethread = true;
#if defined(_MSC_VER)
            wxLogDebug("starting deleting scan thread");
            {
                // wxCriticalSectionLocker enter(m_pThreadCS);
                if (m_scanthread->Delete() != wxTHREAD_NO_ERROR)
                    wxLogError("Can't delete the thread!");
            }

            wxLogDebug("delete func finished thread");
            wxThread::Sleep(1000);
            wxLogDebug("finished Delete() func\n");
            fflush(stdout);
#else
            printf("starting Delete() func\n");
            fflush(stdout);
            {
                wxCriticalSectionLocker enter(m_pThreadCS);
                if (m_scanthread->Delete(NULL, wxTHREAD_WAIT_BLOCK) != wxTHREAD_NO_ERROR)
                    wxLogError("Can't delete the thread!");
            }
            printf("finished Delete() func\n");
            fflush(stdout);
#endif
        }
        Refresh();
        wxLogMessage(wxT("Scan thread stopped."));
    }

    // UpdateStatusBar();
}

void MyFrame::ServerConCloseCheck(wxCommandEvent &WXUNUSED(event))
{
    if (m_sock == NULL || !m_sock->IsConnected())
    {
        MyFrame::ServerConnect();
    }
    else
    {
        MyFrame::ServerCloseCon();
    }
}

/*
    Connect to Corvus Control LabView Server
*/
void MyFrame::ServerConnect()
{
    // if ( m_sock==NULL || !m_sock->IsConnected()) {

    wxIPV4address addr;
    // wxString hostname = wxT("localhost");
    // int port = 3000;
    wxString hostname = m_textCtrlserverip->GetLineText(0);
    wxString port = m_textCtrlserverport->GetLineText(0);
    // wxPrintf("%s:%s", hostname, port);
    int iport = wxAtoi(port);
    addr.Hostname(hostname);
    addr.Service(iport);
    wxLogMessage("Trying to connect to %s:%d", hostname, addr.Service());
    // Wait for the connection event
    m_sock->Connect(addr, false);
    // Wait until the
    // request completes or until we decide to give up
    bool waitmore = true;
    // wait 3sec for connection
    while (!m_sock->WaitOnConnect(3) && waitmore)
    {
        // possibly give some feedback to the user,
        // and update waitmore as needed.
    }
    if (!m_sock->IsConnected())
    {
        wxLogMessage("Cannot connect to server.");
    }
    // }
    UpdateStatusBar();
}

void MyFrame::ServerCloseCon()
{
    m_sock->Close();
    wxLogMessage("Connection to server closed");
    UpdateStatusBar();
}

/*
    Handle Coruvs Control Socket events
*/
void MyFrame::OnSocketEvent(wxSocketEvent &event)
{
    // The socket that had the event
    wxSocketBase *sock = event.GetSocket();
    // Common buffer shared by the events
    char buf[128];
    switch (event.GetSocketEvent())
    {
    case wxSOCKET_CONNECTION:
    {
        wxLogMessage("... socket is now connected.");
        m_timer->Start(TIMEASKPOS);
        UpdateStatusBar();
        // wxMessageBox("Connection to server established",
        //     "Server connectet", wxOK | wxICON_INFORMATION);
        break;
    }
    case wxSOCKET_INPUT:
    {
        memset(buf, 0, sizeof(buf));
        sock->Read(buf, sizeof(buf));
        // printf("%s\n",buf); fflush(stdout);
        wxString s = wxString::FromUTF8(buf);
        s.Trim();
        s << ',';

        // POS? gests back: x.xxx,x.xxx,x.xxx,x
        if (m_getpos)
        {
            bool error = false;
            // get actual position request format is: <xpos> <ypos> <zpos>
            wxStringTokenizer token(s, ",");
            // todo check if has 3 entries
            int i = 0;
            wxTextCtrl *wxtextfields[] = {m_textCtrlXPos, m_textCtrlYPos, m_textCtrlZPos};
            while (token.HasMoreTokens() && i < 3)
            {
                wxString subs = token.GetNextToken();
                // wxCharBuffer buffer = subs.ToUTF8();
                // printf("positions: axis %d  %s\n", i, buffer.data() );
                // fflush(stdout);
                double val;
                if (!subs.ToDouble(&val))
                {
                    error = true;
                    break;
                }
                m_fxyzpos[i] = val;
                m_xyzpos[i] = int(m_fxyzpos[i]);
                wxtextfields[i]->Clear();
                (*(wxtextfields[i])) << subs;
                i++;
            }
            // next token is the Corvu status
            if (token.HasMoreTokens() && !error)
            {
                wxString subs = token.GetNextToken();
                m_status = wxAtoi(subs);
                m_textCtrlStatus->Clear();
                *m_textCtrlStatus << subs;
            }
            m_getpos = false;
        }
        else if (m_scanongoing)
        {
        }
        else
        {
            wxLogMessage("Server sent len=%d: \n\t%s", int(s.Len()), buf);
        }
        break;
    }
    // The server hangs up after sending the data
    case wxSOCKET_LOST:
    {
        wxLogMessage("Connection to server lost");
        UpdateStatusBar();
        break;
    }
    default:
        wxLogMessage("Unknown socket event!!!");
        break;
    }
}

void MyFrame::CorvusSendCmd(wxString s, bool printdebug)
{
    m_sendok = false;
    // adding "\r\n" for LabviewServer
    s << "\r\n";
    wxCharBuffer buf = s.ToUTF8();
    // Send the characters to the server
    if (m_sock)
    {

        if (m_sock->IsConnected())
        {
            if (printdebug)
                wxLogMessage("sending to server: %s", s);
            m_sock->Write(buf.data(), strlen(buf.data()));
        }
        else
        {
            wxLogMessage("cannot send data to servers");
        }
    }
    m_sendok = true;
}

/*
    Send command to Corvus Control Socket
*/
void MyFrame::CorvusSendbtn(wxCommandEvent &WXUNUSED(event))
{
    wxString s = m_textCtrlsendcrvcommand->GetLineText(0);
    CorvusSendCmd(s, true);
}

void MyFrame::GetActualPos()
{
    if (m_sendok)
    {
        wxString s = wxString("PO?");
        m_getpos = true;
        CorvusSendCmd(s);
    }
}

void MyFrame::UpdateStatusBar()
{
#if wxUSE_STATUSBAR
    wxString s;

    if (m_sock == NULL || !m_sock->IsConnected())
    {
        s = "Not connected";

        m_btnConnectSrv->SetBackgroundColour(*wxGREEN);
        m_btnConnectSrv->SetForegroundColour(*wxBLUE);
        m_btnConnectSrv->SetLabel("Connect Server");
        m_timer->Stop();
    }
    else
    {
        wxIPV4address addr;

        m_sock->GetPeer(addr);
        s.Printf("%s : %d", addr.Hostname(), addr.Service());
        m_btnConnectSrv->SetBackgroundColour(*wxRED);
        m_btnConnectSrv->SetForegroundColour(*wxBLUE);
        m_btnConnectSrv->SetLabel("Disconnect Server");
    }

    SetStatusText(s, 1);

    if (!m_scanongoing)
    {
        m_btnScanAxis->SetBackgroundColour(*wxGREEN);
        m_btnScanAxis->SetForegroundColour(*wxBLUE);
        m_btnScanAxis->SetLabel("Scan Axis");
    }
    else
    {
        m_btnScanAxis->SetBackgroundColour(*wxRED);
        m_btnScanAxis->SetForegroundColour(*wxBLUE);
        m_btnScanAxis->SetLabel("Scan Stop");
    }

#endif // wxUSE_STATUSBAR
}

void MyFrame::OnWorkerEvent(wxThreadEvent &event)
{
    if (m_sendok)
    {
        wxString sends = event.GetString();
        CorvusSendCmd(sends);
    }
}

void MyFrame::OnLogEvent(wxThreadEvent &event)
{
    wxString sends = event.GetString();
    wxLogMessage(sends);
}

/*
    Start a peak to peak measurement
    the measured values are stored into a datafile
    if printdebug true a unfiltered waveform is written to wavedata_raw.dat
                  and a averaged waveform is written to wavedata_avrg.dat
*/
void MyFrame::StartMeas(bool printdebug)
{
    wxLogMessage("Measurement has started");
    m_measongoing = true;

    if (m_drs4status == 1)
    {
        int erval = m_drs4exam->StartPeakPeakMeas(m_numavrgmeas, printdebug);
        if (erval == 0)
        {
            wxLogMessage("Measurement has finished");
        }
        else if (erval == 1)
        {
            wxLogMessage("Triggering NOT succesful, Software Trigger used");
            wxLogMessage("Measurement has finished");
        }
        else
        {
            wxLogMessage("Measurment failed");
        }
    }
    else
    {
        wxThread::This()->Sleep(1000);
        // fill dummy data
        for (int i = 0; i < 1024; i++)
        {
            m_drs4exam->m_time_avg_array[i] = i;
            m_drs4exam->m_wave_avg_array[i] = 10 * sin(4 / 1024. * 2 * M_PI * i) - 0.5 + rand() / (float)RAND_MAX;
        }
        wxLogMessage("No Board connected--> No Measurement ");
    }

    m_measongoing = false;
    m_textCtrlDRS4val->Clear();
    *m_textCtrlDRS4val << m_drs4exam->GetPeakPeakMeasVal();
    m_textCtrlDRS4std->Clear();
    *m_textCtrlDRS4std << m_drs4exam->GetWavestdMeasVal();
}

void MyFrame::OnThreadStartMeas(wxThreadEvent &event)
{
    StartMeas(false); // do not print out files in scan mode
}

void MyFrame::OnThreadStart(wxThreadEvent &event)
{
    wxLogMessage("Scan has started");
    m_scanongoing = true;
    UpdateStatusBar();
    m_timer->Start(TIMEASKPOSFAST); // faster asking for position in scan mode
}

void MyFrame::OnThreadCompletion(wxThreadEvent &event)
{
    wxLogMessage("Scan has stopped");
    m_scanongoing = false;

    wxThread::This()->Sleep(800);
    wxLogMessage("thread deleted: %s", m_scanthread ? "NO" : "YES");
    UpdateStatusBar();
    m_timer->Start(TIMEASKPOS);
}

void MyFrame::MoveAbsolute(long xyzmoveloc[3])
{
    wxString loc;
    // absolute move command is MA=x.xxx,x.xxx,x.xxx
    loc << "MA=";
    for (int i = 0; i < 3; i++)
    {
        loc << xyzmoveloc[i] << ".000";
        if (i < 2)
            loc << ",";
    }
    CorvusSendCmd(loc);
}

/*******************************************************************************/
/******************************   MyThread    **********************************/
/*******************************************************************************/

MyThread::~MyThread()
{
    wxCriticalSectionLocker enter(m_frame->m_pThreadCS);
    // the thread is being destroyed; make sure not to leave dangling pointers around
    m_frame->m_scanthread = NULL;
    // printf("thread destructor called\n"); fflush(stdout);
}

void MyThread::MoveAbsolute(long xyzmoveloc[3])
{
    wxString sends, log;
    wxThreadEvent event(wxEVT_THREAD, ID_WORKER_THREAD);
    wxThreadEvent eventlog(wxEVT_THREAD, ID_LOG_THREAD);
    long actpos[3];

    sends << "MA=";
    for (int i = 0; i < 3; i++)
    {
        sends << xyzmoveloc[i] << ".000";
        if (i < 2)
            sends << ",";
    }

    event.SetString(sends);
    // send string over Queue to Mainthread
    wxQueueEvent(m_frame, event.Clone());

    WaitForPosition(xyzmoveloc);

    if (!m_exitthread)
    {
        for (int i = 0; i < 3; i++)
        {
            wxCriticalSectionLocker locker(m_frame->m_pThreadCS);
            actpos[i] = m_frame->m_xyzpos[i];
        }

        log.Printf(wxT("absolute move: %ld %ld %ld"), actpos[0], actpos[1], actpos[2]);
        eventlog.SetString(log);
        // send string over Queue to Mainthread
        wxQueueEvent(m_frame, eventlog.Clone());
    }
}

void MyThread::MoveRelative(float relval, int axisnum)
{
    wxString sends, log;
    wxThreadEvent event(wxEVT_THREAD, ID_WORKER_THREAD);
    wxThreadEvent eventlog(wxEVT_THREAD, ID_LOG_THREAD);
    float actpos = 0, movpos;

    {
        wxCriticalSectionLocker locker(m_frame->m_pThreadCS);
        movpos = m_frame->m_fxyzpos[axisnum] + relval;
    }

    // sends << "MR=";
    //  for (int i=0; i<3; i++) {
    //      sends << actpos[i] << ' ' ;
    //  }
    sends.Printf("MR=%.3f,%d", relval, axisnum + 1); // x-axis = 1, y=2, z=3

    event.SetString(sends);
    // send string over Queue to Mainthread
    wxQueueEvent(m_frame, event.Clone());

    wxStopWatch sw;
    do
    {
        if (TestDestroy())
        {
            m_exitthread = true;
            break;
        }
        wxThread::Sleep(400);

        {
            wxCriticalSectionLocker locker(m_frame->m_pThreadCS);
            actpos = m_frame->m_fxyzpos[axisnum];
        }

    } while (fabs((actpos - movpos) * 1000.f) > TOLMOVE && sw.Time() < 5000);
    sw.Pause();

    if (!m_exitthread)
    {
        log.Printf(wxT("axis %d: progress: %.3f"), axisnum, actpos);
        eventlog.SetString(log);
        // send string over Queue to Mainthread
        wxQueueEvent(m_frame, eventlog.Clone());
    }
}

int MyThread::GetPosition(int axis)
{
    int actpos = -1;
    if (axis < 3 && axis >= 0)
    {
        wxCriticalSectionLocker locker(m_frame->m_pThreadCS);
        actpos = m_frame->m_xyzpos[axis];
    }
    return actpos;
}

bool MyThread::WaitForPosition(long xyzpos[3])
{
    wxStopWatch sw;
    bool posreached = false;
    long actpos[3];
    while (!posreached && sw.Time() < 5000)
    {
        if (TestDestroy())
        {
            m_exitthread = true;
            break;
        }
        posreached = true;
        for (int i = 0; i < 3; i++)
        {
            wxCriticalSectionLocker locker(m_frame->m_pThreadCS);
            actpos[i] = m_frame->m_xyzpos[i];
            if (abs(actpos[i] - xyzpos[i]) * 1000L > TOLMOVE)
            {
                posreached = false;
                break;
            }
        }
        wxThread::This()->Sleep(400);
    }
    sw.Pause();
    return posreached;
}

wxThread::ExitCode MyThread::Entry()
{
    FILE *f;
    long pos[3], startpos[3], endpos[3];
    int steps[3];
    float relstepmove[3];
    int chbox[3];
    long actpos[3];
    wxString sends;

    wxThreadEvent eventstart(wxEVT_THREAD, ID_START_THREAD);
    wxThreadEvent eventlog(wxEVT_THREAD, ID_LOG_THREAD);

    wxThreadEvent eventstartmeas(wxEVT_THREAD, ID_STARTMEAS_THREAD);

    wxQueueEvent(m_frame, eventstart.Clone());

    sends = wxString("Thread started (priority = %u).", GetPriority());
    eventlog.SetString(sends);
    // send string over Queue to Mainthread
    wxQueueEvent(m_frame, eventlog.Clone());

    {
        wxCriticalSectionLocker locker(m_frame->m_pThreadCS);
        for (int i = 0; i < 3; i++)
        {
            chbox[i] = m_frame->m_checkBoxa[i];
            startpos[i] = m_frame->m_posStart[i];
        }
    }
    // wait for positioning exit thread if not possible
    m_exitthread = !WaitForPosition(startpos);
    for (int i = 0; i < 3; i++)
    {
        wxCriticalSectionLocker locker(m_frame->m_pThreadCS);
        actpos[i] = m_frame->m_xyzpos[i];
    }
    sends.Printf(wxT("absolute move: %ld %ld %ld"), actpos[0], actpos[1], actpos[2]);
    eventlog.SetString(sends);

    // wait for reaching start position reached
    for (int i = 0; i < 3; i++)
    {
        if (TestDestroy())
        {
            m_exitthread = true;
            break;
        }
        if (chbox[i])
        {
            {
                wxCriticalSectionLocker locker(m_frame->m_pThreadCS);
                pos[i] = startpos[i] = m_frame->m_xyzpos[i];
                if (m_frame->m_stepsize[i] > 0.01)
                    steps[i] = abs(m_frame->m_posEnd[i] - m_frame->m_posStart[i]) / m_frame->m_stepsize[i];
                else
                {
                    steps[i] = 0.f;
                }
                relstepmove[i] = m_frame->m_posEnd[i] > m_frame->m_posStart[i] ? m_frame->m_stepsize[i] : -m_frame->m_stepsize[i];
                endpos[i] = m_frame->m_posEnd[i];
            }

            sends.Printf(wxT("axis: %d -- start:%ld  end:%ld steps: %d stepsize:%.3f"), i, startpos[i], endpos[i],
                         steps[i], relstepmove[i]);
            eventlog.SetString(sends);
            wxQueueEvent(m_frame, eventlog.Clone());

            /************************************/
            // make check on endpos and stepsize
            /***********************************/
        }
        else
        {
            endpos[i] = 0;
            relstepmove[i] = steps[i] = 0.f;
            pos[i] = startpos[i] = m_frame->m_xyzpos[i];
        }
    }

    f = fopen("scan.dat", "w");
    fprintf(f, "#  Number of measurements (averages) on each position: %d\n", m_frame->GetNumMeasurements());
    fprintf(f, "# x:%ld - %ld   steps: %d   stepsize: %.3f\n", startpos[0],
            endpos[0],
            steps[0],
            relstepmove[0]);
    fprintf(f, "# y:%ld - %ld   steps: %d   stepsize: %.3f\n", startpos[1],
            endpos[1],
            steps[1],
            relstepmove[1]);
    fprintf(f, "# z:%ld - %ld   steps: %d   stepsize: %.3f\n", startpos[2],
            endpos[2],
            steps[2],
            relstepmove[2]);
    fprintf(f, "#  avrg, std:   every sample point is averaged over n Measurements\n");
    fprintf(f, "#  avrg2, std2: peak peak value of each measurement (1024) is averaged\n");
    fprintf(f, "# x pos(um)      y pos(um)      z pos(um)   avrg Vpp(mV)    std (mV)    avrg2 Vpp(mV)  std2(mV)\n");
    fclose(f);

    // move selected axes
    int stepx, stepy, stepz;
    pos[2] = startpos[2];
    stepz = 1;
    MoveAbsolute(pos);
    do
    {
        // check if just this thread was asked to exit
        if (TestDestroy() || m_exitthread)
        {
            m_exitthread = true;
            break;
        }
        // if y axis is checked set x and y back to origin, leave z
        if (chbox[1])
        {
            {
                wxCriticalSectionLocker locker(m_frame->m_pThreadCS);
                pos[2] = m_frame->m_xyzpos[2];
            }
            pos[0] = startpos[0];
            pos[1] = startpos[1];
            MoveAbsolute(pos);
        }
        stepy = 1;
        do
        {
            if (TestDestroy() || m_exitthread)
            {
                m_exitthread = true;
                break;
            }

            // if x is checked set it back to origin, leave the others
            if (chbox[0])
            {
                {
                    wxCriticalSectionLocker locker(m_frame->m_pThreadCS);
                    for (int i = 0; i < 3; i++)
                    {
                        pos[i] = m_frame->m_xyzpos[i];
                    }
                }
                pos[0] = startpos[0];
                MoveAbsolute(pos);
            }
            stepx = 1;
            do
            {
                if (TestDestroy() || m_exitthread)
                {
                    m_exitthread = true;
                    break;
                }
                // Do Measurement and file write in inner loop
                wxQueueEvent(m_frame, eventstartmeas.Clone());

                do
                {
                    if (TestDestroy() || m_exitthread)
                    {
                        m_exitthread = true;
                        break;
                    }
                    wxThread::Sleep(300);
                    // printf("x axis: waiting for measurement to finish\n"); fflush(stdout);
                } while (m_frame->m_measongoing);

                // write to file
                f = fopen("scan.dat", "a");
                {
                    wxCriticalSectionLocker locker(m_frame->m_pThreadCS);
                    fprintf(f, "%+15.3f  %+15.3f  %+15.3f  %+15.6f  %+15.6f  %+15.6f  %+15.6f\n",
                            m_frame->m_fxyzpos[0],
                            m_frame->m_fxyzpos[1],
                            m_frame->m_fxyzpos[2],
                            m_frame->m_drs4exam->GetPeakPeakMeasVal(),
                            m_frame->m_drs4exam->GetWavestdMeasVal(),
                            m_frame->m_drs4exam->GetPeakPeakAvrgVal(),
                            m_frame->m_drs4exam->GetPeakPeakStdVal());
                }
                fclose(f);

                if (chbox[0])
                {
                    // move on to next measure point on x-axis
                    if (stepx <= steps[0])
                        MoveRelative(relstepmove[0], 0);
                }
            } while (stepx++ <= steps[0] && !m_exitthread);

            // skip y axis if not selected
            if (chbox[1])
            {
                if (stepy <= steps[1])
                    MoveRelative(relstepmove[1], 1);
            }
        } while (stepy++ <= steps[1] && !m_exitthread);

        // skip if z checkbox not selected
        if (chbox[2])
        {
            // move relative in z direction
            if (stepz <= steps[2])
                MoveRelative(relstepmove[2], 2);
        }
    } while (stepz++ <= steps[2] && !m_exitthread);

    if (!chbox[0] && !chbox[1] && !chbox[2])
    {
        printf("no checkbox selected. returning...\n");
        fflush(stdout);
    }
    if (m_exitthread)
    {
        printf("thread exit signal received.\n");
        fflush(stdout);
        wxLogDebug("thread almost finished");
    }
    sends = wxT("Thread finished.");
    eventlog.SetString(sends);

    // send string over Queue to Mainthread
    wxQueueEvent(m_frame, eventlog.Clone());

    wxThreadEvent eventfinish(wxEVT_THREAD, ID_FINISH_THREAD);
    wxQueueEvent(m_frame, eventfinish.Clone());

    return (wxThread::ExitCode)0; // success;
}

/*******************************************************************************/
/*****************************   MyPlotFrame     *******************************/
/*******************************************************************************/

/*
    if plotshow is selected a plot frame with the actual data is shown
*/
MyPlotFrame::MyPlotFrame(MyFrame *frame) : framePlot(NULL)
{
    m_frame = frame;

    m_vectorLayer = new mpFXYVector(wxT("DRS4 Signal"));
    m_vectorLayer->ShowName(false);

    Replot();

    m_vectorLayer->SetContinuity(true);
    wxPen vectorpen(*wxBLACK, 2, wxPENSTYLE_SOLID);
    m_vectorLayer->SetPen(vectorpen);
    m_vectorLayer->SetDrawOutsideMargins(false);

    wxFont graphFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
    m_plot = new mpWindow(m_panelplotthis, -1, wxPoint(0, 0), wxSize(400, 300), wxSUNKEN_BORDER);

    // wxColour grey(96, 96, 96);
    // m_plot->SetColourTheme(*wxBLACK, *wxWHITE, grey);

    mpScaleX *xaxis = new mpScaleX(wxT("X"), mpALIGN_BOTTOM, true, mpX_NORMAL);
    mpScaleY *yaxis = new mpScaleY(wxT("Y"), mpALIGN_LEFT, true);
    xaxis->SetFont(graphFont);
    yaxis->SetFont(graphFont);
    xaxis->SetDrawOutsideMargins(false);
    yaxis->SetDrawOutsideMargins(false);
    // Fake axes formatting to test arbitrary format string
    // xaxis->SetLabelFormat(wxT("%.2f €"));
    // yaxis->SetLabelFormat(wxT("%p"));
    m_plot->SetMargins(30, 30, 100, 100);
    //     m_plot->SetMargins(50, 50, 200, 150);
    m_plot->AddLayer(xaxis);
    m_plot->AddLayer(yaxis);
    // m_plot->AddLayer(     new MySIN( 10.0, 220.0 ) );
    // m_plot->AddLayer(     new MyCOSinverse( 10.0, 100.0 ) );
    // m_plot->AddLayer( l = new MyLissajoux( 125.0 ) );
    m_plot->AddLayer(m_vectorLayer);
    //   m_plot->AddLayer(     new mpText(wxT("mpText sample"), 10, 10) );
    wxBrush hatch(wxColour(200, 200, 200), wxBRUSHSTYLE_SOLID);
    // m_plot->AddLayer( nfo = new mpInfoLayer(wxRect(80,20,40,40), &hatch));
    m_plot->AddLayer(nfo = new mpInfoCoords(wxRect(80, 20, 10, 10), wxTRANSPARENT_BRUSH)); //&hatch));
    nfo->SetVisible(false);
    wxBrush hatch2(wxColour(163, 208, 212), wxBRUSHSTYLE_SOLID);
    mpInfoLegend *leg;
    m_plot->AddLayer(leg = new mpInfoLegend(wxRect(200, 20, 40, 40), wxTRANSPARENT_BRUSH)); //&hatch2));
    leg->SetVisible(true);

    // // m_plot->EnableCoordTooltip(true);
    // // set a nice pen for the lissajoux
    // wxPen mypen(*wxRED, 5, wxSOLID);
    // l->SetPen( mypen);

    // wxBoxSizer *hsizer = new wxBoxSizer( wxHORIZONTAL );
    wxBoxSizer *topsizer = new wxBoxSizer(wxVERTICAL);

    topsizer->Add(m_plot, 1, wxEXPAND);
    // topsizer->Add( 0, 0, wxEXPAND|wxALL, 10  );

    //    hsizer->Add( topsizer, 1, wxEXPAND);

    SetAutoLayout(TRUE);
    m_panelplotthis->SetSizer(topsizer);
    //    SetSizer( hsizer );
    axesPos[0] = 0;
    axesPos[1] = 0;
    ticks = true;

    ((mpScaleX *)(m_plot->GetLayer(0)))->SetTicks(ticks);
    ((mpScaleY *)(m_plot->GetLayer(1)))->SetTicks(ticks);

    // m_plot->SetMPScrollbars(false);
    m_plot->Fit();

    m_timer = new wxTimer(this, PLOTTIMER_ID);
    m_timer->SetOwner(this, PLOTTIMER_ID);
    m_timer->Start(TIMEASKPOS);
}

MyPlotFrame::~MyPlotFrame()
{
    delete m_timer;
    m_plot->DelAllLayers(true);
    delete m_plot;
    // delete nfo;
}

void MyPlotFrame::Replot()
{
    std::vector<double> vectorx, vectory;

    while (m_frame->m_measongoing)
    {
    };
    for (int i = 0; i < 1024; i++)
    {
        vectorx.push_back(m_frame->m_drs4exam->m_time_avg_array[i]);
        vectory.push_back(m_frame->m_drs4exam->m_wave_avg_array[i]);
    }
    m_vectorLayer->SetData(vectorx, vectory);
}

void MyPlotFrame::OnTimer(wxTimerEvent &event)
{
    Replot();
    m_plot->Fit();
    m_plot->UpdateAll();
}