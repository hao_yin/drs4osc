///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.9.0 Jan 12 2021)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "drs4corvusforms.h"

///////////////////////////////////////////////////////////////////////////

frameMain::frameMain( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizerFrameMain;
	bSizerFrameMain = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizerMainFrame;
	bSizerMainFrame = new wxBoxSizer( wxHORIZONTAL );

	m_panelMain = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxVERTICAL );

	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 0, 4, 0, 0 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_ALL );

	wxGridSizer* gSizer1;
	gSizer1 = new wxGridSizer( 8, 2, 0, 0 );


	gSizer1->Add( 0, 30, 1, wxEXPAND, 5 );


	gSizer1->Add( 0, 30, 1, wxEXPAND, 5 );

	m_statictextsrvip = new wxStaticText( m_panelMain, wxID_ANY, wxT("Server IP"), wxDefaultPosition, wxDefaultSize, 0 );
	m_statictextsrvip->Wrap( -1 );
	gSizer1->Add( m_statictextsrvip, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 20 );

	m_textCtrlserverip = new wxTextCtrl( m_panelMain, wxID_ANY, wxT("localhost"), wxDefaultPosition, wxDefaultSize, wxTE_RICH2|wxTE_RIGHT );
	gSizer1->Add( m_textCtrlserverip, 0, wxALL, 5 );

	m_staticTextsrvport = new wxStaticText( m_panelMain, wxID_ANY, wxT("Server PORT"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextsrvport->Wrap( -1 );
	gSizer1->Add( m_staticTextsrvport, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_LEFT|wxLEFT, 20 );

	m_textCtrlserverport = new wxTextCtrl( m_panelMain, wxID_ANY, wxT("6345"), wxDefaultPosition, wxDefaultSize, wxTE_RIGHT );
	gSizer1->Add( m_textCtrlserverport, 0, wxALL, 5 );

	m_btnConnectSrv = new wxButton( m_panelMain, ID_SERVER, wxT("Connect Server"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_btnConnectSrv, 0, wxALL|wxEXPAND, 5 );


	gSizer1->Add( 0, 20, 0, 0, 5 );


	gSizer1->Add( 0, 20, 1, wxEXPAND, 5 );


	gSizer1->Add( 0, 0, 1, wxEXPAND, 5 );

	m_staticTextsendcommand = new wxStaticText( m_panelMain, wxID_ANY, wxT("Send Command"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTextsendcommand->Wrap( -1 );
	gSizer1->Add( m_staticTextsendcommand, 0, wxALL, 8 );

	m_textCtrlsendcrvcommand = new wxTextCtrl( m_panelMain, wxID_ANY, wxT("???"), wxDefaultPosition, wxDefaultSize, wxTE_RICH2|wxTE_RIGHT );
	gSizer1->Add( m_textCtrlsendcrvcommand, 0, wxALL, 5 );

	m_btnSendCrvCommand = new wxButton( m_panelMain, ID_CORVUSSEND, wxT("Send Command"), wxDefaultPosition, wxDefaultSize, 0 );
	gSizer1->Add( m_btnSendCrvCommand, 0, wxALL, 5 );

	wxBoxSizer* bSizer152;
	bSizer152 = new wxBoxSizer( wxVERTICAL );

	m_staticText29 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Send ??? to show \ncommands"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText29->Wrap( -1 );
	bSizer152->Add( m_staticText29, 0, wxALL, 5 );


	gSizer1->Add( bSizer152, 1, wxEXPAND, 5 );


	fgSizer1->Add( gSizer1, 0, wxALL|wxEXPAND, 0 );

	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxVERTICAL );

	m_staticText4 = new wxStaticText( m_panelMain, wxID_ANY, wxT("CorvusClient Log:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText4->Wrap( -1 );
	bSizer16->Add( m_staticText4, 0, wxALL|wxEXPAND, 5 );

	m_textCtrlSrvLog = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY );
	m_textCtrlSrvLog->SetMinSize( wxSize( 350,-1 ) );
	m_textCtrlSrvLog->SetMaxSize( wxSize( 350,-1 ) );

	bSizer16->Add( m_textCtrlSrvLog, 1, wxALL|wxEXPAND, 5 );


	fgSizer1->Add( bSizer16, 1, wxALL|wxEXPAND, 10 );

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxVERTICAL );


	bSizer11->Add( 0, 0, 1, wxEXPAND, 5 );


	fgSizer1->Add( bSizer11, 1, wxEXPAND, 5 );

	wxFlexGridSizer* fgSizer4;
	fgSizer4 = new wxFlexGridSizer( 0, 2, 0, 30 );
	fgSizer4->SetFlexibleDirection( wxBOTH );
	fgSizer4->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText30 = new wxStaticText( m_panelMain, wxID_ANY, wxT("DRS4Board status"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText30->Wrap( -1 );
	fgSizer4->Add( m_staticText30, 0, wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );

	m_textCtrlDrs4Status = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxTE_RICH2|wxTE_RIGHT );
	fgSizer4->Add( m_textCtrlDrs4Status, 0, wxALL, 5 );

	m_staticText31 = new wxStaticText( m_panelMain, wxID_ANY, wxT("  Measured pk-pk value"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText31->Wrap( -1 );
	fgSizer4->Add( m_staticText31, 0, wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer1632;
	bSizer1632 = new wxBoxSizer( wxHORIZONTAL );

	m_textCtrlDRS4val = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxTE_RICH2|wxTE_RIGHT );
	bSizer1632->Add( m_textCtrlDRS4val, 0, wxALL, 5 );

	m_staticText322 = new wxStaticText( m_panelMain, wxID_ANY, wxT("mV"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText322->Wrap( -1 );
	bSizer1632->Add( m_staticText322, 0, wxALL, 8 );


	fgSizer4->Add( bSizer1632, 1, wxEXPAND, 5 );

	m_staticText311 = new wxStaticText( m_panelMain, wxID_ANY, wxT("  Measured max std"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText311->Wrap( -1 );
	fgSizer4->Add( m_staticText311, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxBoxSizer* bSizer163;
	bSizer163 = new wxBoxSizer( wxHORIZONTAL );

	m_textCtrlDRS4std = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxTE_RICH2|wxTE_RIGHT );
	bSizer163->Add( m_textCtrlDRS4std, 0, wxALL, 5 );

	m_staticText32 = new wxStaticText( m_panelMain, wxID_ANY, wxT("mV"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText32->Wrap( -1 );
	bSizer163->Add( m_staticText32, 0, wxALL, 8 );


	fgSizer4->Add( bSizer163, 1, wxEXPAND, 5 );

	m_btnSetTriggerLvl = new wxButton( m_panelMain, wxID_ANY, wxT("Set Trigger Level"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer4->Add( m_btnSetTriggerLvl, 0, wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer1631;
	bSizer1631 = new wxBoxSizer( wxHORIZONTAL );

	m_textCtrlDRS4setTrigVal = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER|wxTE_RICH2|wxTE_RIGHT );
	bSizer1631->Add( m_textCtrlDRS4setTrigVal, 0, wxALL, 5 );

	m_staticText321 = new wxStaticText( m_panelMain, wxID_ANY, wxT("mV"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText321->Wrap( -1 );
	bSizer1631->Add( m_staticText321, 0, wxALL, 8 );


	fgSizer4->Add( bSizer1631, 1, wxEXPAND, 5 );

	m_staticText33 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Set Trigger Channel to"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText33->Wrap( -1 );
	fgSizer4->Add( m_staticText33, 0, wxALIGN_CENTER_VERTICAL|wxALL|wxEXPAND, 5 );

	m_cmboTriggerChannel = new wxComboBox( m_panelMain, wxID_ANY, wxT("1"), wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	m_cmboTriggerChannel->Append( wxT("Ch 1") );
	m_cmboTriggerChannel->Append( wxT("Ch 2") );
	m_cmboTriggerChannel->Append( wxT("Ch 3") );
	m_cmboTriggerChannel->Append( wxT("Ch 4") );
	m_cmboTriggerChannel->Append( wxT("EXT") );
	m_cmboTriggerChannel->SetSelection( 0 );
	m_cmboTriggerChannel->Enable( false );

	fgSizer4->Add( m_cmboTriggerChannel, 0, wxALL, 5 );

	m_staticText34 = new wxStaticText( m_panelMain, wxID_ANY, wxT("apply internal \n100MHz signal"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText34->Wrap( -1 );
	fgSizer4->Add( m_staticText34, 0, wxALL, 5 );

	m_chkboxApply100Mhzsig = new wxCheckBox( m_panelMain, wxID_ANY, wxT("enable"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer4->Add( m_chkboxApply100Mhzsig, 0, wxALL, 5 );

	m_staticText331 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Measure on Channel"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText331->Wrap( -1 );
	fgSizer4->Add( m_staticText331, 0, wxALL, 5 );

	m_cmboMeasureChannel = new wxComboBox( m_panelMain, wxID_ANY, wxT("4"), wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	m_cmboMeasureChannel->Append( wxT("Ch 1") );
	m_cmboMeasureChannel->Append( wxT("Ch 2") );
	m_cmboMeasureChannel->Append( wxT("Ch 3") );
	m_cmboMeasureChannel->Append( wxT("Ch 4") );
	m_cmboMeasureChannel->SetSelection( 3 );
	m_cmboMeasureChannel->Enable( false );

	fgSizer4->Add( m_cmboMeasureChannel, 0, wxALL, 5 );

	m_staticText3312 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Num of Averages"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3312->Wrap( -1 );
	fgSizer4->Add( m_staticText3312, 0, wxALL, 5 );

	m_cmboNumAvrgMeas = new wxComboBox( m_panelMain, wxID_ANY, wxT("16"), wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	m_cmboNumAvrgMeas->Append( wxT("1") );
	m_cmboNumAvrgMeas->Append( wxT("2") );
	m_cmboNumAvrgMeas->Append( wxT("4") );
	m_cmboNumAvrgMeas->Append( wxT("8") );
	m_cmboNumAvrgMeas->Append( wxT("16") );
	m_cmboNumAvrgMeas->Append( wxT("32") );
	m_cmboNumAvrgMeas->Append( wxT("64") );
	m_cmboNumAvrgMeas->Append( wxT("128") );
	m_cmboNumAvrgMeas->Append( wxT("256") );
	m_cmboNumAvrgMeas->Append( wxT("512") );
	m_cmboNumAvrgMeas->Append( wxT("1024") );
	m_cmboNumAvrgMeas->SetSelection( 4 );
	m_cmboNumAvrgMeas->Enable( false );

	fgSizer4->Add( m_cmboNumAvrgMeas, 0, wxALL, 5 );

	m_staticText3311 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Get Measurement"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3311->Wrap( -1 );
	fgSizer4->Add( m_staticText3311, 0, wxALL, 5 );

	m_btnGetDRSMeasure = new wxButton( m_panelMain, wxID_ANY, wxT("Measure"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer4->Add( m_btnGetDRSMeasure, 0, wxALL, 5 );

	m_staticText33111 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Remove Spikes"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText33111->Wrap( -1 );
	fgSizer4->Add( m_staticText33111, 0, wxALL, 5 );

	m_chkboxRemoveSpikes = new wxCheckBox( m_panelMain, wxID_ANY, wxT("enable"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer4->Add( m_chkboxRemoveSpikes, 0, wxALL, 5 );


	fgSizer1->Add( fgSizer4, 1, wxEXPAND, 5 );


	bSizer9->Add( fgSizer1, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer162;
	bSizer162 = new wxBoxSizer( wxVERTICAL );


	bSizer162->Add( 0, 10, 0, wxEXPAND, 5 );


	bSizer9->Add( bSizer162, 0, 0, 5 );

	wxBoxSizer* bSizer15;
	bSizer15 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText37 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Actual Position:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText37->Wrap( -1 );
	bSizer15->Add( m_staticText37, 0, wxALL, 5 );

	wxFlexGridSizer* fgSizer3;
	fgSizer3 = new wxFlexGridSizer( 2, 4, 0, 20 );
	fgSizer3->SetFlexibleDirection( wxBOTH );
	fgSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText5134 = new wxStaticText( m_panelMain, wxID_ANY, wxT("X-Axis"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_HORIZONTAL );
	m_staticText5134->Wrap( -1 );
	fgSizer3->Add( m_staticText5134, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );

	m_staticText5132 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Y-Axis"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_HORIZONTAL );
	m_staticText5132->Wrap( -1 );
	fgSizer3->Add( m_staticText5132, 0, wxALL|wxEXPAND, 5 );

	m_staticText5133 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Z-Axis"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_HORIZONTAL );
	m_staticText5133->Wrap( -1 );
	fgSizer3->Add( m_staticText5133, 0, wxALL|wxEXPAND, 5 );

	m_staticText51331 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Status"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTER_HORIZONTAL );
	m_staticText51331->Wrap( -1 );
	fgSizer3->Add( m_staticText51331, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizer161;
	bSizer161 = new wxBoxSizer( wxHORIZONTAL );

	m_textCtrlXPos = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxTE_RIGHT );
	bSizer161->Add( m_textCtrlXPos, 0, wxALL, 5 );

	m_staticText44 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText44->Wrap( -1 );
	bSizer161->Add( m_staticText44, 0, wxALIGN_CENTER|wxALL, 5 );


	fgSizer3->Add( bSizer161, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer1611;
	bSizer1611 = new wxBoxSizer( wxHORIZONTAL );

	m_textCtrlYPos = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxTE_RIGHT );
	bSizer1611->Add( m_textCtrlYPos, 0, wxALL, 5 );

	m_staticText441 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText441->Wrap( -1 );
	bSizer1611->Add( m_staticText441, 0, wxALIGN_CENTER|wxALL, 5 );


	fgSizer3->Add( bSizer1611, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer1612;
	bSizer1612 = new wxBoxSizer( wxHORIZONTAL );

	m_textCtrlZPos = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxTE_RIGHT );
	bSizer1612->Add( m_textCtrlZPos, 0, wxALL, 5 );

	m_staticText442 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText442->Wrap( -1 );
	bSizer1612->Add( m_staticText442, 0, wxALIGN_CENTER|wxALL, 5 );


	fgSizer3->Add( bSizer1612, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer16121;
	bSizer16121 = new wxBoxSizer( wxHORIZONTAL );

	m_textCtrlStatus = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY|wxTE_RIGHT );
	bSizer16121->Add( m_textCtrlStatus, 0, wxALL, 5 );


	fgSizer3->Add( bSizer16121, 1, wxEXPAND, 5 );


	bSizer15->Add( fgSizer3, 1, wxEXPAND, 5 );


	bSizer9->Add( bSizer15, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer1621;
	bSizer1621 = new wxBoxSizer( wxVERTICAL );


	bSizer1621->Add( 0, 10, 0, wxEXPAND, 5 );

	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText28 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Load Actual Pos into Start"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText28->Wrap( -1 );
	bSizer14->Add( m_staticText28, 0, wxALL, 8 );

	m_btnLoadPosintoStart = new wxButton( m_panelMain, wxID_ANY, wxT("Load Pos to Start"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer14->Add( m_btnLoadPosintoStart, 0, wxALL, 5 );


	bSizer1621->Add( bSizer14, 1, wxEXPAND, 5 );


	bSizer1621->Add( 0, 10, 0, 0, 5 );


	bSizer9->Add( bSizer1621, 0, 0, 5 );

	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 0, 8, 0, 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_checkBoxXuse = new wxCheckBox( m_panelMain, wxID_ANY, wxT("X-Scan"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_checkBoxXuse, 0, wxALL, 5 );

	m_staticText53 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Move X from "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText53->Wrap( -1 );
	fgSizer2->Add( m_staticText53, 0, wxALL, 5 );

	m_textCtrlXfrom = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_RICH2|wxTE_RIGHT );
	fgSizer2->Add( m_textCtrlXfrom, 0, wxALL, 5 );

	m_staticText51 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um   to"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText51->Wrap( -1 );
	fgSizer2->Add( m_staticText51, 0, wxALL, 5 );

	m_textCtrlXto = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_RIGHT );
	fgSizer2->Add( m_textCtrlXto, 0, wxALL, 5 );

	m_staticText513 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um   Stepsize:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText513->Wrap( -1 );
	fgSizer2->Add( m_staticText513, 0, wxALL, 5 );

	m_textCtrlXstepsize = new wxTextCtrl( m_panelMain, wxID_ANY, wxT("1"), wxDefaultPosition, wxDefaultSize, wxTE_RIGHT );
	fgSizer2->Add( m_textCtrlXstepsize, 0, wxALL, 5 );

	m_staticText514 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText514->Wrap( -1 );
	fgSizer2->Add( m_staticText514, 0, wxALL, 5 );

	m_checkBoxYuse = new wxCheckBox( m_panelMain, wxID_ANY, wxT("Y-Scan"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_checkBoxYuse, 0, wxALL, 5 );

	m_staticText5 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Move Y from "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText5->Wrap( -1 );
	fgSizer2->Add( m_staticText5, 0, wxALL, 5 );

	m_textCtrlYfrom = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_RIGHT );
	fgSizer2->Add( m_textCtrlYfrom, 0, wxALL, 5 );

	m_staticText512 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um   to"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText512->Wrap( -1 );
	fgSizer2->Add( m_staticText512, 0, wxALL, 5 );

	m_textCtrlYto = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_RIGHT );
	fgSizer2->Add( m_textCtrlYto, 0, wxALL, 5 );

	m_staticText5131 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um   Stepsize:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText5131->Wrap( -1 );
	fgSizer2->Add( m_staticText5131, 0, wxALL, 5 );

	m_textCtrlYstepsize = new wxTextCtrl( m_panelMain, wxID_ANY, wxT("1"), wxDefaultPosition, wxDefaultSize, wxTE_RIGHT );
	fgSizer2->Add( m_textCtrlYstepsize, 0, wxALL, 5 );

	m_staticText5141 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText5141->Wrap( -1 );
	fgSizer2->Add( m_staticText5141, 0, wxALL, 5 );

	m_checkBoxZuse = new wxCheckBox( m_panelMain, wxID_ANY, wxT("Z-Scan"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_checkBoxZuse, 0, wxALL, 5 );

	m_staticText52 = new wxStaticText( m_panelMain, wxID_ANY, wxT("Move Z from "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText52->Wrap( -1 );
	fgSizer2->Add( m_staticText52, 0, wxALL, 5 );

	m_textCtrlZfrom = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_RIGHT );
	fgSizer2->Add( m_textCtrlZfrom, 0, wxALL, 5 );

	m_staticText511 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um   to"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText511->Wrap( -1 );
	fgSizer2->Add( m_staticText511, 0, wxALL, 5 );

	m_textCtrlZto = new wxTextCtrl( m_panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_RIGHT );
	fgSizer2->Add( m_textCtrlZto, 0, wxALL, 5 );

	m_staticText51311 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um   Stepsize:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText51311->Wrap( -1 );
	fgSizer2->Add( m_staticText51311, 0, wxALL, 5 );

	m_textCtrlZstepsize = new wxTextCtrl( m_panelMain, wxID_ANY, wxT("1"), wxDefaultPosition, wxDefaultSize, wxTE_RIGHT );
	fgSizer2->Add( m_textCtrlZstepsize, 0, wxALL, 5 );

	m_staticText51411 = new wxStaticText( m_panelMain, wxID_ANY, wxT("um"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText51411->Wrap( -1 );
	fgSizer2->Add( m_staticText51411, 0, wxALL, 5 );

	m_btnScanAxis = new wxButton( m_panelMain, wxID_ANY, wxT("Scan Axis"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_btnScanAxis, 0, wxALL, 5 );

	m_btnMoveStart = new wxButton( m_panelMain, wxID_ANY, wxT("Move to Start"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizer2->Add( m_btnMoveStart, 0, wxALL, 5 );


	bSizer9->Add( fgSizer2, 1, wxEXPAND, 5 );


	m_panelMain->SetSizer( bSizer9 );
	m_panelMain->Layout();
	bSizer9->Fit( m_panelMain );
	bSizerMainFrame->Add( m_panelMain, 1, wxEXPAND | wxALL, 0 );


	bSizerFrameMain->Add( bSizerMainFrame, 1, wxALL|wxEXPAND, 0 );


	this->SetSizer( bSizerFrameMain );
	this->Layout();
	m_menubarMain = new wxMenuBar( 0 );
	m_menufile = new wxMenu();
	wxMenuItem* m_menuitemfilequit;
	m_menuitemfilequit = new wxMenuItem( m_menufile, wxID_EXIT, wxString( wxT("Quit") ) + wxT('\t') + wxT("Alt+F4"), wxEmptyString, wxITEM_NORMAL );
	m_menufile->Append( m_menuitemfilequit );

	m_menubarMain->Append( m_menufile, wxT("File") );

	m_menuedit = new wxMenu();
	wxMenuItem* m_menuItemShowPlot;
	m_menuItemShowPlot = new wxMenuItem( m_menuedit, wxID_ANY, wxString( wxT("Show Plot") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuedit->Append( m_menuItemShowPlot );

	m_menubarMain->Append( m_menuedit, wxT("Edit") );

	m_menuhelp = new wxMenu();
	wxMenuItem* m_menuabout;
	m_menuabout = new wxMenuItem( m_menuhelp, wxID_ANY, wxString( wxT("About") ) , wxEmptyString, wxITEM_NORMAL );
	m_menuhelp->Append( m_menuabout );

	m_menubarMain->Append( m_menuhelp, wxT("Help") );

	this->SetMenuBar( m_menubarMain );

	m_statusBar1 = this->CreateStatusBar( 2, wxSTB_SIZEGRIP, wxID_ANY );

	this->Centre( wxBOTH );

	// Connect Events
	m_btnConnectSrv->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::ServerConCloseCheck ), NULL, this );
	m_btnSendCrvCommand->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::CorvusSendbtn ), NULL, this );
	m_btnSetTriggerLvl->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::OnSetTrigLvlbtn ), NULL, this );
	m_textCtrlDRS4setTrigVal->Connect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( frameMain::OnSetTrigLvlbtn ), NULL, this );
	m_cmboTriggerChannel->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( frameMain::OnComboSetTriggerChannel ), NULL, this );
	m_chkboxApply100Mhzsig->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( frameMain::OnApply100MhzSig ), NULL, this );
	m_cmboMeasureChannel->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( frameMain::OnComboSetMeasureChannel ), NULL, this );
	m_cmboNumAvrgMeas->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( frameMain::OnComboSetNumAvrgMeas ), NULL, this );
	m_btnGetDRSMeasure->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::OnDRSGetMeasure ), NULL, this );
	m_chkboxRemoveSpikes->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( frameMain::OnRemoveSpikes ), NULL, this );
	m_btnLoadPosintoStart->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::OnLoadPostoStartbtn ), NULL, this );
	m_btnScanAxis->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::ScanAxis ), NULL, this );
	m_btnMoveStart->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::MoveStart ), NULL, this );
	m_menufile->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( frameMain::OnExit ), this, m_menuitemfilequit->GetId());
	m_menuedit->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( frameMain::OnShowPlot ), this, m_menuItemShowPlot->GetId());
	m_menuhelp->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( frameMain::OnAbout ), this, m_menuabout->GetId());
}

frameMain::~frameMain()
{
	// Disconnect Events
	m_btnConnectSrv->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::ServerConCloseCheck ), NULL, this );
	m_btnSendCrvCommand->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::CorvusSendbtn ), NULL, this );
	m_btnSetTriggerLvl->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::OnSetTrigLvlbtn ), NULL, this );
	m_textCtrlDRS4setTrigVal->Disconnect( wxEVT_COMMAND_TEXT_ENTER, wxCommandEventHandler( frameMain::OnSetTrigLvlbtn ), NULL, this );
	m_cmboTriggerChannel->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( frameMain::OnComboSetTriggerChannel ), NULL, this );
	m_chkboxApply100Mhzsig->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( frameMain::OnApply100MhzSig ), NULL, this );
	m_cmboMeasureChannel->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( frameMain::OnComboSetMeasureChannel ), NULL, this );
	m_cmboNumAvrgMeas->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( frameMain::OnComboSetNumAvrgMeas ), NULL, this );
	m_btnGetDRSMeasure->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::OnDRSGetMeasure ), NULL, this );
	m_chkboxRemoveSpikes->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( frameMain::OnRemoveSpikes ), NULL, this );
	m_btnLoadPosintoStart->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::OnLoadPostoStartbtn ), NULL, this );
	m_btnScanAxis->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::ScanAxis ), NULL, this );
	m_btnMoveStart->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( frameMain::MoveStart ), NULL, this );

}

framePlot::framePlot( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxVERTICAL );

	m_panelplotthis = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	bSizer21->Add( m_panelplotthis, 1, wxEXPAND | wxALL, 5 );

	m_btnplotexit = new wxButton( this, wxID_EXIT, wxT("Exit"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer21->Add( m_btnplotexit, 0, wxALL, 5 );


	bSizer20->Add( bSizer21, 1, wxEXPAND, 5 );


	this->SetSizer( bSizer20 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_btnplotexit->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( framePlot::OnClose ), NULL, this );
}

framePlot::~framePlot()
{
	// Disconnect Events
	m_btnplotexit->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( framePlot::OnClose ), NULL, this );

}
