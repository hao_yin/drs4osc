cmake_minimum_required(VERSION 3.11 FATAL_ERROR)
project(drs_core VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
set(INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)

set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)

set(SOURCES
    ${SOURCE_DIR}/averager.cpp
    ${SOURCE_DIR}/DRS.cpp
    ${SOURCE_DIR}/musbstd.c
    ${SOURCE_DIR}/mxml.c
    ${SOURCE_DIR}/rb.cpp
    ${SOURCE_DIR}/strlcpy.c
    ${SOURCE_DIR}/power_supply.cpp
)

set(HEADERS
    ${INCLUDE_DIR}/drs_core/averager.h
    ${INCLUDE_DIR}/drs_core/DRS.h
    ${INCLUDE_DIR}/drs_core/musbstd.h
    ${INCLUDE_DIR}/drs_core/mxml.h
    ${INCLUDE_DIR}/drs_core/rb.h
    ${INCLUDE_DIR}/drs_core/strlcpy.h
    ${INCLUDE_DIR}/drs_core/usb.h
    ${INCLUDE_DIR}/drs_core/power_supply.h
    ${INCLUDE_DIR}/libusb-1.0/libusb.h
)

# add_executable("ps_test"
#     ${SOURCE_DIR}/main_ps_test.cpp
#     ${INCLUDE_DIR}/drs_core/power_supply.h
#     ${INCLUDE_DIR}/drs_core/tcp_connector.h
# )

# target_include_directories("ps_test" PRIVATE ${INCLUDE_DIR})
# target_link_libraries("ps_test" PRIVATE
#     nlohmann_json::nlohmann_json
# )

add_library(${PROJECT_NAME} STATIC ${SOURCES} ${HEADERS})
set_target_properties(${PROJECT_NAME} PROPERTIES PUBLIC_HEADER "${HEADERS}")
target_include_directories(${PROJECT_NAME} PUBLIC ${INCLUDE_DIR})

find_package(wxWidgets COMPONENTS core)
include(${wxWidgets_USE_FILE})

add_dependencies(${PROJECT_NAME} wxWidgets_external)
target_compile_definitions(${PROJECT_NAME} PRIVATE
    WIN32
    HAVE_USB
    HAVE_LIBUSB10
    USE_DRS_MUTEX
)

target_link_libraries(${PROJECT_NAME} PRIVATE 
    nlohmann_json::nlohmann_json
    ${CMAKE_CURRENT_SOURCE_DIR}/lib/libusb-1.0.lib
    ${wxWidgets_LIBRARIES}
)