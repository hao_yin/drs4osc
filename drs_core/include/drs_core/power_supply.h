#pragma once

#include <string>
#include <cstdint>
#include <exception>

#include <iostream>
#include <sstream>
#include <nlohmann/json.hpp>

#include "drs_core/power_supply.h"
#include "drs_core/tcp_connector.h"

namespace power_supply
{
    namespace rpc
    {
        /**
         * @brief Exceptions issued with RPC in general.
         */
        class RPCError : public std::runtime_error
        {
        public:
            RPCError(const std::string &msg);
            ~RPCError() override;
        };

        /**
         * @brief JSONRPC header.
         *
         * The version is set to "0.2".
         */
        struct RPCHeader
        {
            std::string version = "0.2";
        };

        /**
         * @brief RPC call without any parameters.
         *
         * used with Requests and Notifications.
         */
        struct NullParam
        {
        };

        /**
         * @brief JSONRPC Request structure.
         *
         * The request contains:
         *
         * - `method`: function name to be called
         * - `id`: unsigned integer, echoed by the server in its reply
         * - `PARAMETER_T`: argument struct containing the arguments
         *
         * @tparam PARAMETER_T Argument type.
         *
         * The type must have a function overload converting the structure from and to json:
         *
         * - `void to_json(nlohmann::json &json, const PARAMETER_T &request);`
         * - `void from_json(const nlohmann::json &json, PARAMETER_T &request);`
         *
         */
        template <typename PARAMETER_T>
        struct Request : public RPCHeader
        {
            /**
             * @brief Construct a new Request object.
             *
             * @param method method/function name to be called
             * @param id function call id, id 0 is reserved!
             * @param params structure contaning the paramters with from_json and to to_json overload.
             */
            Request(std::string call_name, int call_id, PARAMETER_T params)
                : method(call_name), parameters(params), id(call_id)
            {
                assert(!method.empty());
            }

            std::string method = std::string(); /** function name */
            PARAMETER_T parameters{};           /** parameter type */
            int id = 0;                         /** call id, echoed by the server */
        };

        /**
         * @brief General call with parameters.
         *
         * @tparam PARAMETER_T paramter type
         * @param json Json builder
         * @param request request object
         *
         * @note Requests w/o paramters can be built with `Request<NullParam>`!
         */
        template <typename PARAMETER_T>
        inline auto
        to_json(nlohmann::json &json, const Request<PARAMETER_T> &request) noexcept -> void
        {
            json = nlohmann::json{
                {"method", request.method},
                {"jsonrpc", request.version},
                {"id", request.id},
                {"params", request.parameters},
            };
        }

        /**
         * @brief Requests w/o paramters.
         */
        template <>
        inline auto
        to_json(nlohmann::json &json, const Request<NullParam> &request) noexcept -> void
        {
            assert(!request.method.empty());
            json = nlohmann::json{
                {"method", request.method},
                {"jsonrpc", request.version},
                {"id", request.id},
            };
        }

        /**
         * @brief JSONRPC Notification structure.
         *
         * The notification contains:
         *
         * - `method`: function name to be called
         * - `PARAMETER_T`: argument struct containing the arguments
         *
         * @note Notifications does not have a corresponding server response.
         *
         * @tparam PARAMETER_T Argument type.
         *
         * The type must have a function overload converting the structure from and to json:
         *
         * - `void to_json(nlohmann::json &json, const PARAMETER_T &request);`
         * - `void from_json(const nlohmann::json &json, PARAMETER_T &request);`
         *
         * * @note Requests w/o paramters can be built with `Request<NullParam>`!
         */
        template <typename PARAMETER_T>
        struct Notification : public RPCHeader
        {
            Notification(std::string call_name, PARAMETER_T params) noexcept
                : method(call_name), parameters(params)
            {
            }

            std::string method = std::string(); /** function name */
            PARAMETER_T parameters{};           /** parameter type */
        };

        /**
         * @brief General notification with parameters.
         *
         * @tparam PARAMETER_T paramter type
         * @param json Json builder
         * @param notification request object
         *
         * @note Notification w/o paramters can be built with `Notification<NullParam>`!
         */
        template <typename PARAMETER_T>
        inline auto
        to_json(nlohmann::json &json, const Notification<PARAMETER_T> &notification) noexcept -> void
        {
            assert(!notification.method.empty());
            json = nlohmann::json{
                {"method", notification.method},
                {"jsonrpc", notification.version},
                {"params", notification.parameters},
            };
        }

        /**
         * @brief Notification w/o paramters.
         */
        template <>
        inline auto
        to_json(nlohmann::json &json, const Notification<NullParam> &notification) noexcept -> void
        {
            assert(!notification.method.empty());
            json = nlohmann::json{{"method", notification.method}};
        }

        /**
         * @brief JSONRPC response structure.
         *
         * Each JSONRPC Request must reply with an response containin the following payload:
         *
         * - id (optional): caller id, is set to `null` if the request did not contain an id
         * - jsonrpc: jsonrpc version
         * - result (optional): return value - a nested json object
         *
         * In case of an error the result entry is omitted and replaced with an `error` structure.
         * The error structure contains:
         *
         * - code: error code
         * - message: error msgs
         * - data (optional): additional data
         *
         * @note The error case is handled in the `from_json<Result_t>`. In case of an error RPCError
         * is thrown.
         *
         * @tparam RESULT_T
         */
        template <typename RESULT_T>
        struct Response : public RPCHeader
        {
            int id = 0;
            RESULT_T result{};
        };

        /**
         * @brief Converts the json object to a `Response<RESULT_T>`.
         *
         * @tparam RESULT_T the result structure with it's own `from_json` overload.
         * @param json JSON object
         * @param respose Response object containg the result payload.
         */
        template <typename RESULT_T>
        inline auto from_json(const nlohmann::json &json, Response<RESULT_T> &response) -> void
        {
            auto dump = json.dump();
            if (!json.contains("id") || !json.contains("jsonrpc"))
            {
                auto msg = std::stringstream();
                msg << "Invalid Response, `id` and/or `jsonrpc` nodes are missing! Got: " << json.dump();
                throw RPCError(msg.str());
            }
            json.at("id").get_to(response.id);
            json.at("jsonrpc").get_to<std::string>(response.version);
            if (!json.contains("result"))
            {
                auto msg = std::stringstream();
                if (!json.contains("error"))
                {
                    auto err = std::string();
                    json.at("error").at("message").get_to(err);
                    msg << "Power supply error:" << err;
                }
                else
                {
                    msg << "Invalid Response, `result` node is missing! Got: " << json.dump();
                }
                throw RPCError(msg.str());
            }
            json.at("result").get_to(response.result);
        }

        template <>
        inline auto from_json(const nlohmann::json &json, Response<NullParam> &response) -> void
        {
            auto dump = json.dump();
            if (!json.contains("id") || !json.contains("jsonrpc"))
            {
                auto msg = std::stringstream();
                msg << "Invalid Response, `id` and/or `jsonrpc` nodes are missing! Got: " << json.dump();
                throw RPCError(msg.str());
            }
            json.at("id").get_to(response.id);
            json.at("jsonrpc").get_to<std::string>(response.version);
            if (!json.contains("result"))
            {
                auto msg = std::stringstream();
                if (!json.contains("error"))
                {
                    auto err = std::string();
                    json.at("error").at("message").get_to(err);
                    msg << "Power supply error:" << err;
                }
                else
                {
                    msg << "Invalid Response, `result` node is missing! Got: " << json.dump();
                }
                throw RPCError(msg.str());
            }
        }

        struct Configure
        {
            std::string measurement_type = "iv";
            float begin_voltage = 0.f;
            float end_voltage = 10.f;
            float step_voltage = 1.f;
            float waiting_time = 0.25f;
            float compliance = 0.f;
            float waiting_time_continuous = 0.f;
            bool reset = true;
            bool continuous = true;
        };

        void to_json(nlohmann::json &json, const Configure &cfg);

        struct ChangeVoltage
        {
            ChangeVoltage(float voltage /*, float time, uint32_t step */) noexcept;

            float end_voltage = 0.f; /** requested peak voltage */
            // float waiting_time = 0.f;  /** wait time in seconds. */
            // uint32_t step_voltage = 1; /** number of steps can not be 0!*/
        };

        void to_json(nlohmann::json &json, const ChangeVoltage &cfg);

        struct CurrentState
        {
            std::string state = std::string();
            std::string measurement_type = std::string();
            std::string sample = std::string();
            float source_voltage = 0.f;
            float smu_current = 0.f;
            float elm_current = 0.f;
            float lcr_capacity = 0.f;
            float temperature = 0.f;
        };

        template <typename JSON_TYPE, typename VALUE>
        auto null_default_to(const JSON_TYPE &json, VALUE default) -> VALUE
        {
            return !json.is_null() ? json.get<VALUE>() : default;
        }

        void from_json(const nlohmann::json &json, CurrentState &state);

        template <size_t SIZE>
        class RawJSONBuffer
        {
        public:
            RawJSONBuffer() noexcept = default;

            RawJSONBuffer(RawJSONBuffer &&other) noexcept
                : m_raw_buffer(),
            {
                using std::swap;
                swap(m_raw_buffer, other.m_raw_buffer)
            }

            auto &operator=(RawJSONBuffer &&other) noexcept
            {
                assert(this != &other);

                using std::swap;
                swap(m_raw_buffer, other.m_raw_buffer);
                m_end_of_raw_buffer = other.m_end_of_raw_buffer;
                m_json_buffer = std::move(other.m_end_of_raw_buffer);
                return *this;
            }

            RawJSONBuffer(const RawJSONBuffer &other) = delete;
            auto operator=(const RawJSONBuffer &other) -> RawJSONBuffer & = delete;

            inline auto data() noexcept -> char *
            {
                return m_raw_buffer.data();
            }

            inline auto data() const noexcept -> const char *
            {
                return m_raw_buffer.data();
            }

            inline constexpr auto size() const noexcept -> size_t
            {
                return m_raw_buffer.size();
            }

            inline auto parse_json() -> std::string
            {
                auto it_end_cmd = RawJSONBuffer::is_json_complete(std::begin(m_raw_buffer), std::end(m_raw_buffer));
                if (it_end_cmd == std::end(m_raw_buffer))
                    return std::string();
                return RawJSONBuffer::extract_json(std::begin(m_raw_buffer), it_end_cmd, std::end(m_raw_buffer));
            }

        private:
            static constexpr char OPEN_CHAR{'{'};
            static constexpr char CLOSE_CHAR{'}'};

            using array_iter_t = typename std::array<char, SIZE>::iterator;

            static inline auto is_json_complete(array_iter_t begin, array_iter_t end) -> array_iter_t
            {
                auto oc_brackets = 0u;
                if (*begin != RawJSONBuffer::OPEN_CHAR)
                {
                    auto msg = std::stringstream();
                    msg << "Raw JSON buffer is out of sync, the first char in the raw_buffer must be '{'! "
                        << "Got:" << std::string(begin, std::find(begin, end, char{0}));
                    throw rpc::RPCError(msg.str());
                }

                for (; begin != end; ++begin)
                {
                    oc_brackets += (*begin) == RawJSONBuffer::OPEN_CHAR;
                    oc_brackets -= (*begin) == RawJSONBuffer::CLOSE_CHAR;

                    if (oc_brackets == 0)
                        return begin;

                    if (oc_brackets < 0)
                    {
                        auto msg = std::stringstream();
                        msg << "Invalid JSON! Number '{' is less then '}'! Got: " << std::string(begin, end);
                        throw rpc::RPCError(msg.str());
                    }
                }
                return end;
            }

            static inline auto extract_json(
                array_iter_t begin_array, array_iter_t end_json, array_iter_t end_array) -> std::string
            {
                auto ret = std::string(begin_array, end_json + 1);
                auto it_last = std::copy(end_json + 1, end_array, begin_array);
                std::fill(it_last, end_array, char{0});
                return ret;
            }

        private:
            std::array<char, SIZE> m_raw_buffer{char{0}};
        };

    } // namespace rpc

    enum class CMD_ID : uint32_t
    {
        UNKNOWN = 0,
        START = 1,
        CONFIGURE = 2,
        STATE = 3,
        CHANGE_VOLATGE = 4,
        STOP = 5,
    };

    auto to_string(const CMD_ID &id) -> std::string;

    class Controller
    {
    public:
        Controller() noexcept = default;
        Controller(std::string ip, int16_t port) noexcept;

        auto Configure(const rpc::Configure &cfg) -> void;
        auto State() -> rpc::CurrentState;
        auto ChangeVoltage(const rpc::ChangeVoltage &cfg) -> void;
        auto Stop() -> void;

        inline auto GetIP() noexcept
        {
            return m_ip;
        }

        inline auto GetPort() noexcept
        {
            return m_port;
        }

    private:
        inline auto tcp_socket() -> std::unique_ptr<tcp_socket::TCPStream>
        {
            try
            {
                return tcp_socket::tcp_connect(m_ip, m_port);
            }
            catch (const tcp_socket::TCPError &)
            {
                auto msg = std::stringstream();
                msg << "Can not connect to power supply over '" << m_ip << ':' << m_port << "'!";
                throw rpc::RPCError(msg.str());
            }
        }

    private:
        std::string m_ip = std::string();
        int16_t m_port = 8000;
    };
}