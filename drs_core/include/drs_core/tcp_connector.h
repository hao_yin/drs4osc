#pragma once

#if defined WIN32
#include <winsock.h>
#pragma comment(lib, "ws2_32.lib") // Winsock Library
#else
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif
#include <cstdio>
#include <string>
#include <exception>
#include <chrono>
#include <memory>

namespace tcp_socket
{
    class TCPError : public std::runtime_error
    {
    public:
        TCPError(std::string msg) : std::runtime_error(msg) {}
        ~TCPError() override {}
    };

    inline auto win_clean_up() noexcept -> void
    {
#if defined WIN32
        WSACleanup();
#endif
    }

    inline auto win_start_up() -> void
    {
#if defined WIN32
        auto was_data = WSADATA{};
        int ret = WSAStartup(MAKEWORD(2, 2), &was_data);
        if (ret != 0)
            throw TCPError("Failed to start WSA.");
#endif
    }

    inline auto close(int handle) noexcept -> void
    {
#if defined WIN32
        closesocket(handle);
#else
        close(handle);
#endif
    }

    class TCPStream
    {
    public:
        explicit TCPStream(int handle) noexcept : m_handle(handle) {}
        ~TCPStream()
        {
            if (m_handle != INVALID)
                close(m_handle);
            win_clean_up();
        }

        auto TCPStream::IsReadyToRead(double timeout) noexcept
        {
            assert(timeout >= 0.);

            fd_set socket_handels;
            FD_ZERO(&socket_handels);
            FD_SET(m_handle, &socket_handels);

            struct timeval tv;
            tv.tv_sec = static_cast<long>(timeout);
            tv.tv_usec = static_cast<long>((timeout - tv.tv_sec) * 1e6);

            auto ret = select(m_handle + 1, &socket_handels, nullptr, nullptr, &tv);
            return (ret > 0);
        }

        auto read(char *const data, size_t words, double timeout) -> int64_t
        {
            if (!this->IsReadyToRead(timeout))
                return 0;

            auto ret = recv(m_handle, data, words, 0);
            if (ret == 0)
                throw TCPError("The host closed the connection!");
            if (ret < 0)
                throw TCPError("recv socket error!");

            return ret;
        }

        auto write(const char *const data, size_t words) -> int64_t
        {
            auto ret = send(m_handle, data, words, 0);
            if (ret < 0)
                throw TCPError("recv socket error!");
            return ret;
        }

    private:
#if defined WIN32
        static constexpr int INVALID = INVALID_SOCKET;
#else
        static constexpr int INVALID = -1;
#endif
        int m_handle = INVALID;
    };

    inline auto try_connect(int sock, const sockaddr_in &addr) -> void
    {
        u_long block = 1;
        if (ioctlsocket(sock, FIONBIO, &block) == SOCKET_ERROR)
            throw TCPError("Failed to connect to server!");

        if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) == SOCKET_ERROR)
        {
            if (WSAGetLastError() != WSAEWOULDBLOCK)
                throw TCPError("Failed to connect to server!");

            // connection pending

            fd_set setW, setE;

            FD_ZERO(&setW);
            FD_SET(sock, &setW);
            FD_ZERO(&setE);
            FD_SET(sock, &setE);

            timeval time_out = {0};
            time_out.tv_sec = 1;
            time_out.tv_usec = 0;

            int ret = select(0, NULL, &setW, &setE, &time_out);
            if (ret <= 0)
            {
                // select() failed or connection timed out
                close(sock);
                if (ret == 0)
                    WSASetLastError(WSAETIMEDOUT);
                throw TCPError("Failed to connect to server!");
            }

            if (FD_ISSET(sock, &setE))
            {
                close(sock);
                throw TCPError("Failed to connect to server!");
            }
        }

        // connection successful

        // put socked in blocking mode...
        block = 0;
        if (ioctlsocket(sock, FIONBIO, &block) == SOCKET_ERROR)
        {
            close(sock);
            throw TCPError("Failed to connect to server!");
        }
    }

    inline auto tcp_connect(const std::string &ip, int16_t port) -> std::unique_ptr<TCPStream>
    {
        win_start_up();

        auto addr = sockaddr_in{};
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = inet_addr(ip.c_str());
        addr.sin_port = htons(port);

        auto socket_handle = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (socket_handle < 0)
        {
            close(socket_handle);
            win_clean_up();
            throw TCPError("Failed to create socket handle!");
        }

        if (connect(socket_handle, (struct sockaddr *)&addr, sizeof(addr)) == SOCKET_ERROR)
            throw TCPError("Failed to connect to server! Try to ping the IP address and confirm that the diode-measurement is running!");

        return std::make_unique<TCPStream>(socket_handle);
    }
} // namespace TCP socket
