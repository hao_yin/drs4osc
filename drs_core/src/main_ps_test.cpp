#include "drs_core/power_supply.h"
#include "drs_core/tcp_connector.h"
#include <iostream>
#include <memory>
#include <thread>
#include <chrono>

inline auto operator<<(std::ostream &stream, const power_supply::rpc::CurrentState &state) -> std::ostream &
{
    stream << "CurrentState(state:" << state.state << ", measurement_type:" << state.measurement_type
           << ", sample:" << state.sample << ", source_voltage:" << state.source_voltage
           << ", smu_current:" << state.smu_current << ", elm_current:" << state.elm_current
           << ", lcr_capacity:" << state.lcr_capacity << ", temperature:" << state.temperature << ")";
    return stream;
}

void check_buffer()
{
    power_supply::rpc::RawJSONBuffer<4096ul> buffer;
    auto msg = std::string("{\"result\": {\"state\": \"idle\", \"measurement_type\": null, \"sample\": null, \"source_voltage\": null, \"smu_current\": null, \"elm_current\": null, \"lcr_capacity\": null, \"temperature\": null}, \"id\": 3, \"jsonrpc\": \"2.0\"}");
    assert(buffer.size() > msg.size());
    std::copy(std::begin(msg), std::end(msg), buffer.data());
    std::cout << "befor: " << std::string(buffer.data()) << "\n";
    auto ret = buffer.parse_json();
    auto json_reply = nlohmann::json::parse(ret);
    using Response = power_supply::rpc::Response<power_supply::rpc::CurrentState>;

    Response resp = json_reply.get<Response>();
    std::cout << resp.result << '\n';

    std::cout << "size: " << ret.size() << ", content:" << ret << "\n";
    std::cout << "after: " << std::string(buffer.data()) << "\n";
}

void test_tcp_connector()
{
    auto tcp_stream = tcp_socket::tcp_connect("127.0.0.1", 8000);
    power_supply::rpc::RawJSONBuffer<4096ul> buffer;
    auto msg = std::string("{\"method\":\"start\",\"params\":{\"end_voltage\":1.0},\"jsonrpc\":\"0.2\"}");
    std::cout << "test: " << tcp_stream->write(msg.data(), msg.size()) << '\n';
}

void test_controller()
{
    auto ctrl = std::make_unique<power_supply::Controller>("127.0.0.1", 8000);
    auto cfg = power_supply::rpc::Configure();
    ctrl->Configure(cfg);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    auto state = ctrl->State();
    std::cout << state;
    do
    {
        state = ctrl->State();
        std::cout << state;
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
    } while (state.state == "configure" || state.state == "ramping");

    auto next_voltage = power_supply::rpc::ChangeVoltage(5.f, 0.25f, 1);
    ctrl->ChangeVoltage(next_voltage);

    do
    {
        state = ctrl->State();
        std::cout << state;
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
    } while (state.state == "ramping");

    ctrl->Stop();

    do
    {
        state = ctrl->State();
        std::cout << state;
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
    } while (state.state == "stopping");
}

int main()
{
    // check_buffer();
    // test_tcp_connector();
    test_controller();
}
