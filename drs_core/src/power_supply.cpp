#include "drs_core/power_supply.h"

namespace power_supply
{
    namespace rpc
    {
        RPCError::RPCError(const std::string &msg) : std::runtime_error(msg) {}
        RPCError::~RPCError() {}

        void to_json(nlohmann::json &json, const Configure &cfg)
        {
            json = nlohmann::json{
                {"measurement_type", cfg.measurement_type},
                {"begin_voltage", cfg.begin_voltage},
                {"end_voltage", cfg.end_voltage},
                {"step_voltage", cfg.step_voltage},
                {"waiting_time", cfg.waiting_time},
                {"compliance", cfg.compliance},
                {"waiting_time_continuous", cfg.waiting_time_continuous},
                {"reset", cfg.reset},
                {"continuous", cfg.continuous},
            };
        }

        ChangeVoltage::ChangeVoltage(float voltage /*, float time, uint32_t step*/) noexcept
            : end_voltage(voltage) /*, waiting_time(time), step_voltage(step)*/
        {
            // assert(step_voltage != 0.f);
            // assert(waiting_time >= 0.f);
        }

        void to_json(nlohmann::json &json, const ChangeVoltage &cfg)
        {
            json = nlohmann::json{
                {"end_voltage", cfg.end_voltage},
                // {"step_voltage", cfg.step_voltage},
                // {"waiting_time", cfg.waiting_time},
            };
        }

        void from_json(const nlohmann::json &json, CurrentState &state)
        {
            json.at("state").get_to(state.state);
            state.measurement_type = null_default_to(json.at("measurement_type"), std::string("unknown"));
            state.sample = null_default_to(json.at("sample"), std::string("unknown"));
            state.source_voltage = null_default_to(json.at("source_voltage"), 0.f);
            state.smu_current = null_default_to(json.at("smu_current"), 0.f);
            state.elm_current = null_default_to(json.at("elm_current"), 0.f);
            state.lcr_capacity = null_default_to(json.at("lcr_capacity"), 0.f);
            state.temperature = null_default_to(json.at("temperature"), 0.f);
        }
    } // namespace RPC

    std::string to_string(const CMD_ID &id)
    {
        switch (id)
        {
        case CMD_ID::UNKNOWN:
            return "unknown";
        case CMD_ID::START:
            return "start";
        case CMD_ID::CONFIGURE:
            return "configure";
        case CMD_ID::STATE:
            return "state";
        case CMD_ID::CHANGE_VOLATGE:
            return "change_voltage";
        case CMD_ID::STOP:
            return "stop";
        }
        auto msg = std::stringstream();
        msg << "Invalid command ID, got: " << static_cast<unsigned int>(id) << "!";
        throw rpc::RPCError(msg.str());
    }

    Controller::Controller(std::string ip, int16_t port) noexcept
        : m_ip(ip), m_port(port)
    {
    }

    void Controller::Configure(const rpc::Configure &cfg)
    {
        auto cur_state = this->State();
        if (cur_state.state != "idle")
        {
            throw rpc::RPCError("Power supply is not in `idle`. Please stop the "
                                "`diode-measurement` manually, befor starting the voltage scan.");
        }

        // using Notification = rpc::Notification<decltype(cfg)>;
        // auto request = Notification(to_string(CMD_ID::START), cfg);
        using Request = rpc::Request<decltype(cfg)>;
        auto request = Request(to_string(CMD_ID::START), static_cast<int>(CMD_ID::START), cfg);
        auto json = nlohmann::json(request);
        auto msg = json.dump();

        auto tcp = this->tcp_socket();
        tcp->write(msg.data(), msg.size());

        rpc::RawJSONBuffer<4096> buffer;
        auto sock_ret = int64_t{0};
        try
        {
            for (auto i_ret = 0; (i_ret < 3) && !sock_ret; ++i_ret)
                sock_ret = tcp->read(buffer.data(), buffer.size(), 1.);
        }
        catch (const tcp_socket::TCPError &err)
        {
            throw rpc::RPCError(std::string("Failed read `change_voltage` response from power supply, got: ") + err.what());
        }

        if (sock_ret == 0)
        {
            throw tcp_socket::TCPError("Reading `change_voltage` response has timed out!");
        }
    }

    rpc::CurrentState Controller::State()
    {
        using Request = rpc::Request<rpc::NullParam>;

        auto request = Request(to_string(CMD_ID::STATE), static_cast<int>(CMD_ID::STATE), rpc::NullParam{});
        auto json = nlohmann::json(request);
        auto msg = json.dump();

        auto tcp = this->tcp_socket();

        try
        {
            tcp->write(msg.data(), msg.size());
        }
        catch (tcp_socket::TCPError &err)
        {
            throw rpc::RPCError(std::string("Failed send state command, got: ") + err.what());
        }

        rpc::RawJSONBuffer<4096> buffer;
        auto sock_ret = int64_t{0};
        try
        {
            for (auto i_ret = 0; (i_ret < 3) && !sock_ret; ++i_ret)
                sock_ret = tcp->read(buffer.data(), buffer.size(), 1.);
        }
        catch (const tcp_socket::TCPError &err)
        {
            throw rpc::RPCError(std::string("Failed read `state` response from power supply, got: ") + err.what());
        }

        if (sock_ret == 0)
        {
            throw rpc::RPCError("Read the `state` response has timed out!");
        }

        auto json_reply = nlohmann::json::parse(buffer.parse_json());
        using Response = rpc::Response<rpc::CurrentState>;

        const auto response = json_reply.get<Response>();
        return response.result;
    }

    void Controller::ChangeVoltage(const rpc::ChangeVoltage &cfg)
    {
        auto cur_state = this->State();
        if (cur_state.state != "continuous")
        {
            auto msg = std::stringstream();
            msg << "Power supply is not in `continuous` state. Can not change voltage in `"
                << cur_state.state << "`!";
            throw rpc::RPCError(msg.str());
        }

        using Request = rpc::Request<rpc::ChangeVoltage>;
        auto request = Request(to_string(CMD_ID::CHANGE_VOLATGE), static_cast<int>(CMD_ID::CHANGE_VOLATGE), cfg);
        auto json = nlohmann::json(request);
        auto msg = json.dump();

        auto tcp = this->tcp_socket();
        try
        {
            tcp->write(msg.data(), msg.size());
        }
        catch (tcp_socket::TCPError &err)
        {
            throw rpc::RPCError(std::string("Failed send `change_voltage` command, got: ") + err.what());
        }

        rpc::RawJSONBuffer<4096> buffer;
        auto sock_ret = int64_t{0};
        try
        {
            for (auto i_ret = 0; (i_ret < 3) && !sock_ret; ++i_ret)
                sock_ret = tcp->read(buffer.data(), buffer.size(), 1.);
        }
        catch (const tcp_socket::TCPError &err)
        {
            throw rpc::RPCError(std::string("Failed read `change_voltage` response from power supply, got: ") + err.what());
        }

        if (sock_ret == 0)
        {
            throw rpc::RPCError("Reading `change_voltage` response has timed out!");
        }
    }

    void Controller::Stop()
    {
        auto cur_state = this->State();
        if (cur_state.state == "idle")
            return void();

        using Request = rpc::Request<rpc::NullParam>;
        auto request = Request(to_string(CMD_ID::STOP), static_cast<int>(CMD_ID::STOP), rpc::NullParam{});
        auto json = nlohmann::json(request);
        auto msg = json.dump();

        auto tcp = this->tcp_socket();
        try
        {
            tcp->write(msg.data(), msg.size());
        }
        catch (tcp_socket::TCPError &err)
        {
            throw rpc::RPCError(std::string("Failed send `stop` command, got: ") + err.what());
        }

        rpc::RawJSONBuffer<4096> buffer;
        auto sock_ret = int64_t{0};
        try
        {
            for (auto i_ret = 0; (i_ret < 3) && !sock_ret; ++i_ret)
                sock_ret = tcp->read(buffer.data(), buffer.size(), 1.);
        }
        catch (const tcp_socket::TCPError &err)
        {
            throw rpc::RPCError(std::string("Failed read `stop` response from power supply, got: ") + err.what());
        }

        if (sock_ret == 0)
        {
            throw rpc::RPCError("Reading `change_voltage` response has timed out!");
        }
    }
}